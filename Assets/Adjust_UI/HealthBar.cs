﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public DurabilityComponent durabilityComponent;
    public Image imageHeath;

    void Update()
    {
        float currentHeath = durabilityComponent.getHitPoints();
        float maxHealth = durabilityComponent.MaxHitPoints;

        var theTransform = XUtils.getComponent<RectTransform>(imageHeath, XUtils.AccessPolicy.ShouldExist);

        Vector2 theAnchorMax = theTransform.anchorMax;
        theAnchorMax.x = durabilityComponent.getHitPointsRatio();
        theTransform.anchorMax = theAnchorMax;
    }

}
