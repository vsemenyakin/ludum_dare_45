﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    public int scoreAmount;
    Text scoreText;

    void Awake()
    {
        scoreText = GetComponent<Text>();
        scoreAmount = 0;
    }

    public void SetCountText(int enemyCount) {
        scoreAmount = enemyCount;
        scoreText.text = "Score: " + scoreAmount + "/" + GameController.KILL_ENEMY_FOR_WIN_COUNT;
    }
}
