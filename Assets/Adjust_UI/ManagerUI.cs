﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ManagerUI : MonoBehaviour
{
    public GameObject gamePanel;
    public Button buttonBack;
    public ScoreCounter scoreCounter;

    public Sprite WinSprite;
    public Sprite LoseSprite;
    public Image ResultImage;
    
    void Awake()
    {
        buttonBack.onClick.AddListener(SceneLoad.GoToGameScene);
    }
    
    public void PanelGameSetActive(bool isWin) {
        gamePanel.SetActive(true);

        if (isWin){
            ResultImage.sprite = WinSprite;
        }
        else{
            ResultImage.sprite = LoseSprite;
        }
    }

}
