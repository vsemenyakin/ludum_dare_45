﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.time - startTime;
        string minutes = ((int)t / 60).ToString();
        string seconds = Mathf.Floor(t % 60).ToString("f0");

        if (((int)t / 60) > 0) {
            timerText.text = (minutes + " : " + seconds);
        } else {
            timerText.text = seconds;
        }
    }
}
