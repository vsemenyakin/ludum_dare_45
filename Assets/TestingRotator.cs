﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingRotator : MonoBehaviour
{
    void FixedUpdate() {
        _rotator.setTargetLocation(XUtils.getMouseWorldPosition());
    }

    [SerializeField] LimitedRotatorComponent _rotator = null;
}
