﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour
{
    [SerializeField] private Button _startButton = null;
    
    private void Awake(){
        _startButton.onClick.AddListener(GoToGameScene);
    }


    public static void GoToGameScene(){
        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;
    }
}
