﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public GameObject options;
//    public void OnClickStart () {
//        SceneLoad.LoadScene(1);
//    }

    public void OnClickOptions() {
        options.SetActive(!options.activeSelf);
    }
    
    public void OnClickExit () {
        Application.Quit();
    }

    public void setMusic(float value) {
        Global.music = value;
    }
    public void setSound(float value) {
        Global.sound = value;
    }



}
