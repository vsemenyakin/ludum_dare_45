﻿using System;

[Serializable]
public class EnemyConfigItem {
	public string Id;
	public float MinSpeed;
	public float MaxSpeed;
	public float MinTargetDistance;
	public float MaxTargetDistance;
	public float TargetDistanceToSlowDown;
	public float MinRotateSpeed;
	public float MaxRotateSpeed;
}
