using UnityEngine;

namespace Gameplay.AI {
	[RequireComponent(typeof(CarPhysicsLogic))]
	public class ShipEnemy : Enemy {
		private CarPhysicsLogic _moveController;
		private float _targetSpeed;
		private float _wheelDelta;
		private float _timeSinceLastWheel;

		protected override void Update() {
			base.Update();

			if (_distanceToTarget > _config.MaxTargetDistance) {
				return;
			}

			_moveController.SetMinimumSpeed(_config.MinSpeed);

			var distanceToSlowDown = _config.TargetDistanceToSlowDown;
			if (_distanceToTarget > distanceToSlowDown) {
				_moveController.SetMaximumSpeed(_config.MaxSpeed);
			} else {
				_moveController.SetMaximumSpeed(Mathf.Lerp(_config.MaxSpeed * 0.1f, _config.MaxSpeed * 0.3f,
					_distanceToTarget / (distanceToSlowDown - _config.MinTargetDistance)));
			}

			_wheelDelta = Mathf.DeltaAngle(_rb.rotation, _targetAngle);

			var absAngle = Mathf.Abs(_wheelDelta);

			//set rotate speed
			_moveController.SteeringWheelSpeed =
				Mathf.Lerp(_config.MinRotateSpeed, _config.MaxRotateSpeed, absAngle / 180);

			if (Mathf.Abs(_distanceToTarget - _config.MinTargetDistance) > 0.1f) {
				bool forward = _distanceToTarget > _config.MinTargetDistance ||
				               _distanceToTarget < _config.MinTargetDistance && absAngle > 90;

				if (forward) {
					_moveController.applyGas();
				} else {
					_moveController.applyRevers();
				}
			}


			_timeSinceLastWheel += Time.deltaTime;

			if (Mathf.Abs(_wheelDelta) < 1 || _timeSinceLastWheel < 0.2f) {
				return;
			}

			_timeSinceLastWheel = 0;

			if (_wheelDelta > 0) {
				_moveController.rotateSteeringWheelClockwise();
			} else {
				_moveController.rotateSteeringWheelCounterClockwise();
			}
		}

		protected override void Start() {
			base.Start();
			_moveController = GetComponent<CarPhysicsLogic>();
		}
	}
}