using UnityEngine;

namespace Gameplay.AI {
	public class DumbMovingEnemy : Enemy {
		protected override void UpdateTargetDirection(){
			base.UpdateTargetDirection();
			_rb.rotation = _targetAngle;
			_targetDirection.Normalize();
		}
    
		private void FixedUpdate() {
			if (_distanceToTarget > _config.MaxTargetDistance) {
				return;
			}
			moveCharacter(_targetDirection);
		}
		void moveCharacter(Vector2 direction) {
			MoveSpeed = _config.MaxSpeed * 0.01f;
			if (_distanceToTarget > _config.MinTargetDistance) {
				_rb.MovePosition((Vector2)transform.position + (direction * MoveSpeed * Time.deltaTime));
			}
		}
	}
}