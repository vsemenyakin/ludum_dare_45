﻿using UnityEngine;

[CreateAssetMenu(fileName = "enemies_config", menuName = "Config/Enemies", order = 0)]
public class EnemiesConfig : ScriptableObject {
    public EnemyConfigItem[] Enemies;
}
