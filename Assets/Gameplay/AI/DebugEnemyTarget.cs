using UnityEngine;

public class DebugEnemyTarget : MonoBehaviour, IEnemyTarget {
	public Vector3 GetPosition() {
		return transform.position;
	}
}
