﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyTarget {
    Vector3 GetPosition();
}
