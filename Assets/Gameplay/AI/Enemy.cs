﻿using System;
using UnityEngine;

namespace Gameplay.AI {
    [RequireComponent(typeof(Rigidbody2D))]
    public class Enemy : MonoBehaviour{
        public float MoveSpeed = 5f;
        public IEnemyTarget Target;
        protected Rigidbody2D _rb;
        protected float _targetAngle;
        protected Vector3 _targetDirection;
        protected EnemyConfigItem _config;
        protected float _distanceToTarget;

        protected virtual void Update() {
            UpdateTargetDirection();
        }

        protected virtual void UpdateTargetDirection() {
            _targetDirection = Target.GetPosition() - transform.position;
            _targetAngle = Mathf.Atan2(_targetDirection.y, _targetDirection.x) * Mathf.Rad2Deg;
            

            _distanceToTarget = Mathf.Abs(Vector3.Distance(Target.GetPosition(), transform.position));
            
        }

        // Start is called before the first frame update
        protected virtual void Start(){
            _rb = this.GetComponent<Rigidbody2D>();
        }

        public void SetTarget(IEnemyTarget target) {
            Target = target;
        }

        public void SetConfig(EnemyConfigItem config) {
            _config = config;
        }
    }
}