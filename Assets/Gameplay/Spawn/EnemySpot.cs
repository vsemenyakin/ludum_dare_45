﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpot : MonoBehaviour
{
    [SerializeField] private GameObject _enemyTemplate = null;
    [SerializeField] private int _enemyNum = 0;
    [SerializeField] private float _radiusSpawn = 0f;
    [SerializeField] private bool _spawnOnStart = true;
    [SerializeField] private CircleCollider2D _trigger = null;
    void Start()
    {

        if (_spawnOnStart) {
            Spawn();
            _trigger.enabled = false;
        }

    }

    private void Spawn() {
        Debug.Log("Spawn");
        float step = 360f / _enemyNum;
        for (int cnt = 0; cnt < _enemyNum; cnt++) {
            float curLength = step * cnt;
            float x = -Mathf.Sin((curLength * Mathf.PI) / 180);
            float y = Mathf.Cos((curLength * Mathf.PI) / 180);
            Vector2 diff = new Vector2(x, y);
            diff *= _radiusSpawn;

            Vector3 enemyPos = transform.position;
            enemyPos.x += diff.x;
            enemyPos.y += diff.y;

            Instantiate(_enemyTemplate, enemyPos, Quaternion.Euler(new Vector3(0, 0, curLength)));
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") Spawn();
    }
}
