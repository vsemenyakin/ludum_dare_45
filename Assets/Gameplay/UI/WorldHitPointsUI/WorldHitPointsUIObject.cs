﻿using UnityEngine;

public class WorldHitPointsUIObject : MonoBehaviour
{
    private void Awake() {
        XUtils.check(_hitPointsBarUIPrefab);
    }

    void FixedUpdate() {
        DurabilityComponent[] theDurabilityComponents = FindObjectsOfType<DurabilityComponent>();

        foreach (DurabilityComponent theDurabilityComponent in theDurabilityComponents) {
            if (!XUtils.isValid(theDurabilityComponent)) continue;
            if (!theDurabilityComponent.showLifebarForUI) continue;

            Optional<int> theFoundIndex = new Optional<int>();
            
            for (int theIndex = 0; theIndex < _durabilityInfos.getSize(); ++theIndex) {
                if (theDurabilityComponent == _durabilityInfos[theIndex].durabilityComponent) {
                    theFoundIndex = new Optional<int>(theIndex);
                    break;
                }
            }
            if (!theFoundIndex.isSet()) {
                WorldHitPointUIBarObject theNewBar = Instantiate(_hitPointsBarUIPrefab);
                theNewBar.transform.SetParent(transform, false);

                _durabilityInfos.add(new DurabilityUIInfo(theDurabilityComponent, theNewBar));
            }
        }

        _durabilityInfos.iterateWithRemove((DurabilityUIInfo theInfo)=>{
            if (!XUtils.isValid(theInfo.durabilityComponent)) {
                XUtils.destroy(theInfo.hitPointsBarUI.gameObject);

                return true;
            } else {
                WorldHitPointUIBarObject theBar = theInfo.hitPointsBarUI;
                theBar.set(theInfo.durabilityComponent.getHitPointsRatio());

                var theBarTransform = XUtils.getComponent<RectTransform>(theBar, XUtils.AccessPolicy.ShouldExist);
                Vector2 theAnchorPosition = Camera.main.WorldToViewportPoint(theInfo.durabilityComponent.transform.position);
                theBarTransform.anchorMin = theAnchorPosition;
                theBarTransform.anchorMax = theAnchorPosition;

                return false;
            }
        }, true);
    }

    private struct DurabilityUIInfo {
        public DurabilityUIInfo(
            DurabilityComponent inDurabilityComponent, WorldHitPointUIBarObject inHitPointsBarUI)
        {
            durabilityComponent = inDurabilityComponent;
            hitPointsBarUI = inHitPointsBarUI;
            oldHitPoints = durabilityComponent.getHitPoints();
        }

        public DurabilityComponent durabilityComponent;
        public WorldHitPointUIBarObject hitPointsBarUI;
        public float oldHitPoints;
    }

    public WorldHitPointUIBarObject _hitPointsBarUIPrefab = null;
    private FastArray<DurabilityUIInfo> _durabilityInfos = new FastArray<DurabilityUIInfo>();
}
