﻿using UnityEngine;

public class WorldHitPointUIBarObject : MonoBehaviour
{
    private void Awake() {
        XUtils.check(_hitPointsRectTransform);
    }

    public void set(float inValueRatio) {
        inValueRatio = Mathf.Clamp(inValueRatio, 0f, 1f);
        _hitPointsRectTransform.anchorMax = new Vector2(inValueRatio, 1f);
    }

    public RectTransform _hitPointsRectTransform = null;
}
