﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private GameObject _objecForFollowing;

    private void Start() {
        if (!_objecForFollowing) {
            _objecForFollowing = GameObject.FindGameObjectWithTag("Player");
            if (!_objecForFollowing) _objecForFollowing = GameObject.Find("PlayerShip");
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (XUtils.isValid(_objecForFollowing))
        {
            Vector3 thePosition = _objecForFollowing.transform.position;
            if (_saveZPosition)
                thePosition.z = transform.position.z;

            transform.position = thePosition;
        }
    }

    [SerializeField] bool _saveZPosition = true;
}
