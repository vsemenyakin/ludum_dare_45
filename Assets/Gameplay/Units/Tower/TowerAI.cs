﻿using UnityEngine;
using static XMath;

public class TowerAI : MonoBehaviour
{
    private void Awake()
    {
        XUtils.check(_weapon);
    }

    private void FixedUpdate()
    {
        Vector2 theTargetPosition = getTargetPosition();

        Vector2 thePosition2D = transform.position;
        Vector2 theTargetDirection = theTargetPosition - thePosition2D;

        float theDeltaAngle = getNormalizedAngle(Quaternion.FromToRotation(transform.up, theTargetDirection).eulerAngles.z);

        float theDistance = Vector2.Distance(thePosition2D, theTargetPosition);

        //Applying attack state
        if (_weapon)
        {
            _weapon.setTargetLocation(theTargetPosition);

            if (Mathf.Abs(theDeltaAngle) < _angleForShooting && theDistance < _maxShootingDistance)
                _weapon.shoot();
        }
    }

    private Vector2 getTargetPosition()
    {
        return XUtils.isValid(_targetGameObject) ?
            _targetGameObject.transform.position : Vector3.zero;
    }

    private float getCurrentRotation()
    {
        return transform.rotation.eulerAngles.z;
    }

    //Fields
    [SerializeField] private float _angleForShooting = 10f;
    [SerializeField] private float _maxShootingDistance = 10f;

    [SerializeField] private GameObject _targetGameObject = null;

    [SerializeField] private IWeaponControlInterface _weapon = null;
}
