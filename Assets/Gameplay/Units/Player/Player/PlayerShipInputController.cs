﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerShipInputController : MonoBehaviour
{
    public List<IWeaponControlInterface> weaponObjects = new List<IWeaponControlInterface>();

    private void Start() {
        XUtils.check(weaponObjects);
    }

    private void FixedUpdate() {
        updateShootingInput();
    }

    private void updateShootingInput() {
        foreach (IWeaponControlInterface theGunObject in weaponObjects)
            theGunObject.setTargetLocation(XUtils.getMouseWorldPosition());

        if (Input.GetMouseButton(0))
            foreach (IWeaponControlInterface theGunObject in weaponObjects)
                theGunObject.shoot();
    }
}
