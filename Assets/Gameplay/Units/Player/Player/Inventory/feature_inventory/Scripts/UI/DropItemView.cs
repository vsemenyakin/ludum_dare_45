﻿using UnityEngine;

public class DropItemView : MonoBehaviour {
    [SerializeField] private Collider2D _box = null;

    public string DropId { get; private set; }

    public Collider2D Box {
        get { return _box; }
    }

    public void SetContent(string id) {
        DropId = id;
    }
}
