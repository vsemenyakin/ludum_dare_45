﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryItemView : DragAndDroppable, IDroppable, IPointerClickHandler
{
	public event Action ItemDropped;
	public event Action OnRightClick;

	private string _dropId;

	private WeaponController _weaponController;
	private InventoryController _inventoryController;

	public string DropId => _dropId;

	private void Awake(){
		var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
		_weaponController = gameController.WeaponController;
		_inventoryController = gameController.InventoryController;
	}

	public void SetContent(string dropId){
		_dropId = dropId;
	}

	public bool CanBeDroppedOn(IDropTarget target, PointerEventData eventData){
		var dropTarget = target as InventoryItemDropTarget;

		if (dropTarget != null){
			switch (dropTarget.State){
				case InventoryDropState.Placement:
					return !_weaponController.IsDropWeapon(_dropId);
				case InventoryDropState.Enemy:
					return _weaponController.IsDropWeapon(_dropId);
				case InventoryDropState.Inventory:
					return false;
				default:
					Debug.LogError("state = " + dropTarget.State + " is not found");
					break;
			}
		}

		return false;
	}

	public void DropOn(IDropTarget target, PointerEventData eventData) {
		ItemDropped?.Invoke();
	}

	public override void OnBeginDrag(PointerEventData eventData){
		switch (eventData.button) {
			case PointerEventData.InputButton.Left:
				base.OnBeginDrag(eventData);
				CheckDragForPlacement(true);
				break;
		}
	}

	public override void OnEndDrag(PointerEventData eventData){
		base.OnEndDrag(eventData);
		CheckDragForPlacement(false);
	}

	private void CheckDragForPlacement(bool isBeginDrag){
		var isNotDropWeapon = _dropId != null && !_weaponController.IsDropWeapon(_dropId);
		
		var isItemDraggedForPlacement = isBeginDrag && isNotDropWeapon;

		_inventoryController.IsItemDraggedForPlacement = isItemDraggedForPlacement;
	}

	public void OnPointerClick(PointerEventData eventData){
		switch (eventData.button) {
			case PointerEventData.InputButton.Right:
				OnRightClick?.Invoke();
				break;
		}
	}
}
