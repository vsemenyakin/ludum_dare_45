﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public enum InventoryDropState
{
    Enemy,
    Placement,
    Inventory
}

public class InventoryItemDropTarget : MonoBehaviour, IDropTarget
{
    public InventoryDropState State = InventoryDropState.Placement;
    
    public event Action <string> DropReceived;
    
    public bool CanBeDropped(IDroppable droppable, PointerEventData eventData) {
        return droppable is InventoryItemView;
    }

    public void GiveDropFeedback(IDroppable droppable, PointerEventData eventData) {
    }

    public void ReceiveDrop(IDroppable droppable, PointerEventData eventData) {       
        var dropId = (droppable as MonoBehaviour).gameObject.GetComponent<InventoryItemView>().DropId;
        DropReceived?.Invoke(dropId);
    }
}
