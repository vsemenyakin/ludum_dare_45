
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    [SerializeField] private InventoryController _inventoryController = null;
    public InventoryController InventoryController => _inventoryController;

    [SerializeField] private PlacementController _placementController = null;
    public PlacementController PlacementController => _placementController;

    [SerializeField] private DataController _dataController = null;
    public DataController DataController => _dataController;

    [SerializeField] private WeaponController _weaponController = null;
    public WeaponController WeaponController => _weaponController;

    [SerializeField] private PlayerShipInputController _playerShipInputController = null;
    public PlayerShipInputController PlayerShipInputController => _playerShipInputController;
    [SerializeField] private DropController _dropController = null;
    public DropController DropController => _dropController;

    [SerializeField] private DurabilityComponent _shipDurability = null;

    [SerializeField] private ManagerUI managerUI = null;

    private int killEnemyCount = 0;

    public static int KILL_ENEMY_FOR_WIN_COUNT = 15;

    private void Awake() {
        _shipDurability.OnDead += NotifyOnShipDead;

    }

    private void Start(){
        managerUI.scoreCounter.SetCountText(killEnemyCount);
    }

    public void CountKillEnemy() {
        killEnemyCount++;

        managerUI.scoreCounter.SetCountText(killEnemyCount);

        if(KILL_ENEMY_FOR_WIN_COUNT <= killEnemyCount) {
            EndGame(true);
        }
    }
    public void NotifyOnShipDead(){
        EndGame(false);
    }
    
    public void EndGame(bool isWin) {
        managerUI.PanelGameSetActive(isWin);
        Time.timeScale = 0;
    }
}
