﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class InventoryNotificationView : MonoBehaviour {
	[SerializeField] private TextMeshProUGUI _tf = null;
	[SerializeField] private GameObject _body = null;
	private Coroutine _showCoroutine;
	private float _timeToShow;

	private void Awake() {
		_body.SetActive(false);
	}

	public void SetConfig(float timeToShow) {
		_timeToShow = timeToShow;
	}

	public void ShowMessage(string text) {
		_tf.text = text;
		_body.SetActive(true);
		if (_showCoroutine != null) {
			StopCoroutine(_showCoroutine);
		}
		_showCoroutine = StartCoroutine(ShowMessageCoroutine());
	}

	private IEnumerator ShowMessageCoroutine() {
		yield return new WaitForSeconds(_timeToShow);
		HideMessage();
	}

	public void HideMessage() {
		_body.SetActive(false);
	}
}
