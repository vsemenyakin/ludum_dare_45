﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InventoryController : MonoBehaviour {
    [SerializeField] private InventoryView _view = null;
    [SerializeField] private ItemsFinder _itemsFinder = null;
    private InventoryConfig _config;
    public string DebugPickUpItem;
    
    private InventoryState _state;
    private DropItemView _lastAvailableDrop;

    private bool _isItemDraggedForPlacement;

    public bool IsItemDraggedForPlacement{
        get{ return _isItemDraggedForPlacement; }
        set{
            if (_isItemDraggedForPlacement != value){
                _isItemDraggedForPlacement = value;
                DragForPlacementChanged?.Invoke(_isItemDraggedForPlacement);
            }
        }
    }

    public event Action<DropItemView> DropPickedUp;
    public event Action<bool> DragForPlacementChanged;

    private void Start() {
        Initialize();
        TryPutItem("powder_barrel");
        TryPutItem("cannon");
    }

    public void Initialize() {
        _config = Resources.Load<InventoryConfig>("Config/inventory_config");
        CreateState();
        
        _view.SetContent(_state, _config);
        _itemsFinder.SetConfig(_config.PickUpDropRadius);
        
        _view.OnSlotRightClick += HandleSlotRightClick;
        _view.OnItemDropped += HandleItemDropped;
    }

    private void HandleSlotRightClick(InventorySlotState slot) {
        TryRemoveItem(slot);
    }

    private void HandleItemDropped(InventorySlotState slot){
        TryRemoveItem(slot);
    }

    private void CreateState() {
        _state = new InventoryState {
            Slots = new List<InventorySlotState>()
        };

        for (int i = 0; i < _config.SlotsCount; i++) {
            _state.Slots.Add(new InventorySlotState {
                ItemId = null
            });
        }
    }

    // Update is called once per frame
    void Update() {
        var availableDrop = GetAvailableDrop();

        if (availableDrop != null && string.IsNullOrEmpty(availableDrop.DropId)) {
            _lastAvailableDrop = null;
            _view.HideMessage();
            return;
        }

        if (_lastAvailableDrop != availableDrop) {
            _view.ShowMessage(_config.CanPickUpMessage);
        }
        
        _lastAvailableDrop = availableDrop;
        if (Input.GetKeyDown(KeyCode.E)) {
            if (TryPickUpItem()){
                NotifyDropPickedUp();
            }
        }
    }

    private DropItemView GetAvailableDrop() {
        var item = _itemsFinder.GetAvailableItem();
        return item;
    }

    public bool TryPutItem(string itemId){
        var availableSlot = GetAvailableSlot();
        
        if (availableSlot == null) {
            _view.ShowMessage(_config.NoSpaceMessage);
            return false;
        }

        availableSlot.ItemId = itemId;
        
        _view.UpdateView();

        return true;
    }

    private bool TryPickUpItem() {
        var availableSlot = GetAvailableSlot();

        if (availableSlot == null) {
            _view.ShowMessage(_config.NoSpaceMessage);
            return false;
        }

        if (_lastAvailableDrop == null){
            return false;
        }
        
        availableSlot.ItemId = _lastAvailableDrop.DropId;
        
        _view.UpdateView();

        return true;
    }

    private void NotifyDropPickedUp(){
        DropPickedUp?.Invoke(_lastAvailableDrop);
    }
    
    private void TryRemoveItem(InventorySlotState slot) {
        slot.ItemId = null;
        
        _view.UpdateView();
    }

    private InventorySlotState GetAvailableSlot() {
        return _state.Slots.FirstOrDefault(s => s.ItemId == null);
    }
}