﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemsFinder : MonoBehaviour {
    [SerializeField] private DropPickupArea _ship = null;

    private DropController _dropController;


    public DropItemView GetAvailableItem() {
        float minDistance = float.MaxValue;
        DropItemView closestDrop = null;

        var drop = _dropController.Drop;
        
        foreach (var dropItem in drop) {
            if (!dropItem || !dropItem.Box || !_ship || !_ship.Box) continue;

            var distance = dropItem.Box.Distance(_ship.Box).distance;
            if (distance <= _ship.Radius && minDistance > distance) {
                closestDrop = dropItem;
                minDistance = distance;
            }
        }

        return closestDrop;
    }

    public void SetConfig(float radius) {
        _ship.SetConfig(radius);
    }

    private void Awake() {
        var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        _dropController = gameController.DropController;
    }
}
