﻿using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

[CustomEditor(typeof(DropPickupArea))]
public class DropPickupAreaEditor : Editor
{
    public void OnSceneGUI() {
        var area = (target as DropPickupArea);
        var radius = area.Radius;
        var position = area.transform.position;

        Handles.DrawLine(new Vector3(area.Box.bounds.min.x - radius, area.Box.bounds.min.y - radius, 0), new Vector3(area.Box.bounds.max.x + radius, area.Box.bounds.max.y + radius, 0));
        Handles.DrawLine(new Vector3(area.Box.bounds.min.x - radius, area.Box.bounds.max.y + radius, 0), new Vector3(area.Box.bounds.max.x + radius, area.Box.bounds.min.y - radius, 0));
    }
}

#endif
