﻿using System.Collections.Generic;

public class InventoryState
{
    public List<InventorySlotState> Slots = new List<InventorySlotState>();
}

public class InventorySlotState {
    public string ItemId;
}
