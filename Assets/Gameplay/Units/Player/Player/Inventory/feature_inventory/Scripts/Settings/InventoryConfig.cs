using UnityEngine;

[CreateAssetMenu(fileName = "inventory_config", menuName = "Config/Inventory", order = 0)]
public class InventoryConfig : ScriptableObject {
	public int SlotsCount;
	public string NoSpaceMessage;
	public string CanPickUpMessage;
	public float TimeToShowMessage;
	public float PickUpDropRadius;
}