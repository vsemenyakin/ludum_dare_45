﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventorySlotView : MonoBehaviour{
    [SerializeField] private Image _icon = null;
    [SerializeField] private InventoryItemView _itemViewDrop = null;
    
    private string _cachedItemId;
    public InventorySlotState State { get; private set; }
    private bool _initialized;
    public event Action<InventorySlotView> OnRightClick;
    public event Action<InventorySlotView> OnItemDropped;

    private void Awake(){
        _itemViewDrop.ItemDropped += HandleItemDropped;
        _itemViewDrop.OnRightClick += HandleOnRightClick;
    }

    public void SetContent(InventorySlotState state) {
        State = state;
        UpdateView();
    }

    public void UpdateView() {
        if (State == null) {
            gameObject.SetActive(false);
            return;
        }
        
        if (State.ItemId == _cachedItemId && _initialized) {
            return;
        }
        
        gameObject.SetActive(true);
        _cachedItemId = State.ItemId;
        _icon.enabled = _cachedItemId != null;
        _icon.sprite = DataController.GetItemSprite(DataType.Item,_cachedItemId);

        if (_cachedItemId != null){
            _itemViewDrop.SetContent(_cachedItemId);
        }

        _initialized = true;
    }

    private void HandleItemDropped(){
        OnItemDropped?.Invoke(this);
    }

    private void HandleOnRightClick(){
        OnRightClick?.Invoke(this);
    }
}
