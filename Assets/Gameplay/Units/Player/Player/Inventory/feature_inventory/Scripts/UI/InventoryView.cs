﻿using System;
using System.Linq;
using UnityEngine;

public class InventoryView : MonoBehaviour
{
    [SerializeField] private Transform _slotsParent = null;
    [SerializeField] private InventoryNotificationView _notificationView = null;
    private InventorySlotView[] _slots = null;
    public event Action<InventorySlotState> OnSlotRightClick;
    public event Action<InventorySlotState> OnItemDropped; 

    private void Awake() {
        _slots = _slotsParent.GetComponentsInChildren<InventorySlotView>();
        foreach (var slot in _slots) {
            slot.OnRightClick += HandleSlotRightClick;
            slot.OnItemDropped += HandleItemDropped;
        }
    }

    private void HandleSlotRightClick(InventorySlotView slot) {
        OnSlotRightClick?.Invoke(slot.State);
    }
    
    private void HandleItemDropped(InventorySlotView slot) {
        OnItemDropped?.Invoke(slot.State);
    }

    public void SetContent(InventoryState state, InventoryConfig config) {
        for (int i = 0; i < _slots.Length; i++) {
            var slotState = state.Slots.ElementAtOrDefault(i);
            _slots[i].SetContent(slotState);
        }
        
        _notificationView.SetConfig(config.TimeToShowMessage);
    }

    public void UpdateView() {
        foreach (var inventorySlotView in _slots) {
            inventorySlotView.UpdateView();
        }
    }

    public void ShowMessage(string text) {
        _notificationView.ShowMessage(text);
    }

    public void HideMessage() {
        _notificationView.HideMessage();
    }
}
