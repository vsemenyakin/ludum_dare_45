﻿using UnityEngine;

public class DropPickupArea : MonoBehaviour {
    public float Radius;
    [SerializeField] private Collider2D _box = null;

    public Collider2D Box {
        get { return _box; }
    }

    public void SetConfig(float radius) {
        Radius = radius;
    }
    
}
