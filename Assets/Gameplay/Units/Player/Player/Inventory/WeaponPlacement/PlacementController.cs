using System.Collections.Generic;
using UnityEngine;

public class PlacementController : MonoBehaviour
    {
        [SerializeField] private PlacementView _view = null;
        private PlacementState _state;

        private InventoryController _inventoryController;
        private WeaponController _weaponController;
        
        private void Start() {
            Initialize();
        }

        private void Initialize(){
            var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
            _inventoryController = gameController.InventoryController;
            _weaponController = gameController.WeaponController;

            _inventoryController.DragForPlacementChanged += HandleDragForPlacementChanged;
            
            CreateState();

            _view.SetContent(_state);
            _view.OnSlotRightClick += HandleOnSlotRightClick;
            _view.OnDropReceived += HandleOnDropReceived;
        }

        private void HandleOnDropReceived(PlacementSlotView slot, string dropId){
            TryPickUpItem(slot, dropId);
        }

        private void HandleOnSlotRightClick(PlacementSlotView slot){
            TryRemoveItem(slot);
        }

        private void HandleDragForPlacementChanged(bool isDragged){
            _view.SetHighlight(isDragged);
            _view.UpdateView();
        }

        private void TryPickUpItem(PlacementSlotView slot, string dropId){
            var state = slot.State;

            if (state.ItemId == null){
                state.ItemId = dropId;
                _view.UpdateView();

                if (_weaponController.IsWeapon(state.ItemId)){
                    var weaponPrefab = WeaponController.GetWeaponPrefab(state.ItemId);
                    var weaponGO = Instantiate(weaponPrefab, slot.transform, true);
                    weaponGO.transform.localPosition = new Vector3(0,0,0);
                    weaponGO.transform.localRotation = slot.transform.localRotation;
                    slot.placementItemGO = weaponGO;
                    var iWeapon = weaponGO.GetComponent<IWeaponControlInterface>();
                    _weaponController.AddWeapon(iWeapon);
                }
            }
        }

        private void TryRemoveItem(PlacementSlotView slot){
            var state = slot.State;
            if (state.ItemId != null && _inventoryController.TryPutItem(state.ItemId)){
                if (_weaponController.IsWeapon(state.ItemId)){
                    var iWeapon = slot.placementItemGO.GetComponent<IWeaponControlInterface>();
                    _weaponController.RemoveWeapon(iWeapon);
                    Destroy(slot.placementItemGO);
                }
                
                state.ItemId = null;
                _view.UpdateView();
            }
        }
        
        private void CreateState() {
            _state = new PlacementState {
                Slots = new List<PlacementSlotState>()
            };

            var slotCount = _view.SlotCount();
            for (int i = 0; i < slotCount; i++){
                _state.Slots.Add(new PlacementSlotState(){
                    ItemId = null
                });
            }

            //TODO: add placement slot config place ship level
//            for (int i = 0; i < _config.SlotsCount; i++) {
//                _state.Slots.Add(new PlacementSlotState {
//                    ItemId = null
//                });
//                         }
        }
    }
