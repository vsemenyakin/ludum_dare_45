﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlacementView : MonoBehaviour
{
    [SerializeField] private Transform _slotsParent = null;
    private PlacementSlotView[] _slots = null;
    public event Action<PlacementSlotView> OnSlotRightClick;
    public event Action<PlacementSlotView, string> OnDropReceived; 
    
    
    private void Awake() {
        _slots = _slotsParent.GetComponentsInChildren<PlacementSlotView>();
        foreach (var slot in _slots) {
            slot.OnRightClick += HandleSlotRightClick;
            slot.OnDropReceived += HandleOnDropReceived;
        }
    }
    
    private void HandleSlotRightClick(PlacementSlotView slot) {
        OnSlotRightClick?.Invoke(slot);
    }

    private void HandleOnDropReceived(PlacementSlotView slot, string dropId){
        OnDropReceived?.Invoke(slot, dropId);
    }

    public void SetContent(PlacementState state){
        for (int i = 0; i < _slots.Length; i++) {
            var slotState = state.Slots.ElementAtOrDefault(i);
            _slots[i].SetContent(slotState);
        }
    }

    public void SetHighlight(bool isHighlight){
        foreach (var placementSlotView in _slots) {
            placementSlotView.SetHighlight(isHighlight);
        }
        
        UpdateView();
    }
    
    public void UpdateView() {
        foreach (var placementSlotView in _slots) {
            placementSlotView.UpdateView();
        }
    }

    public int SlotCount(){
        return _slots.Length;
    }
}
