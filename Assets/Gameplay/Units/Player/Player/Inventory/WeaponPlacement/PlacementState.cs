using System.Collections.Generic;

public class PlacementState
{
    public List<PlacementSlotState> Slots = new List<PlacementSlotState>();
}

public class PlacementSlotState {
    public string ItemId;
}