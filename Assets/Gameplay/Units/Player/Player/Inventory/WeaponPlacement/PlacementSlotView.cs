using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlacementSlotView : MonoBehaviour, IPointerClickHandler
{
    private const float TURN_ON_SLOT_ALPHA = 0.75f;
    private const float TURN_OFF_SLOT_ALPHA = 25f/255f;
    private const float EFFECT_RANGE = 0.6f;
    
    
    [SerializeField] private SpriteRenderer _icon = null;
    [SerializeField] private SpriteRenderer _visual = null;
    [SerializeField] private InventoryItemDropTarget _dropTarget = null;
    public PlacementSlotState State { get; private set; }
    public event Action<PlacementSlotView> OnRightClick;
    public event Action<PlacementSlotView, string> OnDropReceived;

    public GameObject placementItemGO;
    
    private string _cachedItemId;
    private bool _initialized;

    private bool _isHighlight;
    private float _highlightValue = 0f;

    private void Awake(){
        _dropTarget.DropReceived += HandleDropReceived;
    }

    private void Start(){
        _highlightValue = 0;
        SetHighlightValue(_highlightValue);
    }

    private void Update(){
        if (_isHighlight){
            AnimateHighlight();
        }
    }

    public void SetContent(PlacementSlotState state){
        State = state;
        UpdateView();
    }

    public void SetHighlight(bool isHighlight){
        _isHighlight = isHighlight;
        _highlightValue = 0f;
        UpdateHighlight();
    }

    public void UpdateView(){
        if (State == null) {
            return;
        }

        if (State.ItemId == _cachedItemId && _initialized) {
            return;
        }
        
        _cachedItemId = State.ItemId;

        if (_cachedItemId == null){
            _icon.sprite = Resources.Load<Sprite>($"placement_items/square");
            _dropTarget.gameObject.SetActive(true);
        }
        else{
            _icon.sprite = null;
            _dropTarget.gameObject.SetActive(false);
        }

        _initialized = true;
    }

    private void UpdateHighlight(){
        if (_dropTarget.gameObject.activeSelf || !_isHighlight){
            var colorVisual = _visual.color;

            if (_isHighlight){
                colorVisual.a = TURN_ON_SLOT_ALPHA;
            }
            else{
                colorVisual.a = TURN_OFF_SLOT_ALPHA;
                _highlightValue = 0f;
                SetHighlightValue(_highlightValue);
            }
            
            _visual.color = colorVisual; 
        }
    }

    private void AnimateHighlight(){
        _highlightValue += Time.deltaTime;
        var value = Mathf.PingPong(_highlightValue, 1f - EFFECT_RANGE) + EFFECT_RANGE;
        SetHighlightValue(value);
    }

    private void SetHighlightValue(float value){
        _visual.material.SetFloat("Vector1_506FDAB0", value);
    }

    private void HandleDropReceived(string dropId){
        OnDropReceived?.Invoke(this, dropId);
    }

    public void OnPointerClick(PointerEventData eventData){
        switch (eventData.button) {
            case PointerEventData.InputButton.Right:
                OnRightClick?.Invoke(this);
                break;
        }
    }
}
