﻿using UnityEngine;

public class EnemyFeedAttackerAI : MonoBehaviour
{
    private void Start() {
        ship = gameObject.GetComponent<ShipController>();

        _updateLogic = UpdatePursuit;
    }

    private void FixedUpdate() {
        _updateLogic.Invoke();
    }

    private System.Action _updateLogic = null;

    private void UpdatePursuit() {
        Vector2 theTargetPosition = getTargetPosition();

        Vector2 thePosition2D = transform.position;
        Vector2 theTargetDirection = theTargetPosition - thePosition2D;

        float theDeltaAngle = XMath.getNormalizedAngle(
            Quaternion.FromToRotation(transform.up, theTargetDirection).eulerAngles.z
        );

        float theDistance = Vector2.Distance(thePosition2D, theTargetPosition);

        //Applying state move state
        ship.StartMoving(1f);

        if (theDeltaAngle > _angleForCorrection) {
            ship.Rotate(-1f);
        } else if (theDeltaAngle < -_angleForCorrection) {
            ship.Rotate(1f);
        }

        if (theDistance < _nearestBattleModeStartRadius) {
            _updateLogic = UpdateBattleModeAngleFind;
        }
    }

    private void UpdateBattleModeAngleFind() {
        Vector2 theTargetPosition = getTargetPosition();

        Vector2 thePosition2D = transform.position;
        Vector2 theTargetDirection = theTargetPosition - thePosition2D;

        float theDeltaAngle = XMath.getNormalizedAngle(
            Quaternion.FromToRotation(-transform.up, theTargetDirection).eulerAngles.z
        );

        float theDistance = Vector2.Distance(thePosition2D, theTargetPosition);

        //Logic
        weapon.setTargetLocation(theTargetPosition);

        ship.StartMoving(0.1f);
        if (theDeltaAngle > _angleForAttackStart) {
            ship.Rotate(-1f);
        } else if (theDeltaAngle < -_angleForAttackStart) {
            ship.Rotate(1f);
        } else {
            _updateLogic = UpdateBattleModeAttacking;
        }

        if (theDistance > _pursuitStartRadius) {
            _updateLogic = UpdatePursuit;
        }
    }

    private void UpdateBattleModeAttacking() {
        Vector2 theTargetPosition = getTargetPosition();

        Vector2 thePosition2D = transform.position;
        Vector2 theTargetDirection = theTargetPosition - thePosition2D;

        float theDeltaAngle = XMath.getNormalizedAngle(
            Quaternion.FromToRotation(-transform.up, theTargetDirection).eulerAngles.z
        );

        float theDistance = Vector2.Distance(thePosition2D, theTargetPosition);

        //Logic
        ship.StopMoving();

        weapon.setTargetLocation(theTargetPosition);
        weapon.shoot();

        if (theDistance > _pursuitStartRadius) {
            _updateLogic = UpdatePursuit;
        } else if (Mathf.Abs(theDeltaAngle) > _angleForAttackAngleFindingStart) {
            _updateLogic = UpdateBattleModeAngleFind;
        }
    }

    private Vector2 getTargetPosition() {
        return XUtils.isValid(_targetGameObject) ?
            _targetGameObject.transform.position : Vector3.zero;
    }

    private float getCurrentRotation() {
        return transform.rotation.eulerAngles.z;
    }

    //Fields
    private ShipController ship = null;
    [SerializeField] private IWeaponControlInterface weapon = null;

    [SerializeField] private GameObject _targetGameObject = null;

    //-Pursuit
    [SerializeField] private float _angleForCorrection = 1f;

    //-Battle mode
    [SerializeField] float _pursuitStartRadius = 16f;
    [SerializeField] float _nearestBattleModeStartRadius = 8f;

    [SerializeField] private float _angleForAttackStart = 10f;
    [SerializeField] private float _angleForAttackAngleFindingStart = 45f;
}
