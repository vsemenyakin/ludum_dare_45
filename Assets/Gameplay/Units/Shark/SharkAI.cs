﻿using UnityEngine;

using static DamageSystem;

public class SharkAI : MonoBehaviour
{
    private void Start() {
        ship = gameObject.GetComponent<ShipController>();
    }

    private void FixedUpdate() {
        Vector2 theTargetPosition = getTargetPosition();

        Vector2 thePosition2D = transform.position;
        Vector2 theTargetDirection = theTargetPosition - thePosition2D;

        float theDeltaAngle = XMath.getNormalizedAngle(
            Quaternion.FromToRotation(transform.up, theTargetDirection).eulerAngles.z
        );

        float theDistance = Vector2.Distance(thePosition2D, theTargetPosition);

        //Applying state move state
        ship.StartMoving(1f);

        if (theDeltaAngle > _angleForCorrection) {
            ship.Rotate(-1f);
        } else if (theDeltaAngle < -_angleForCorrection) {
            ship.Rotate(1f);
        }

        //Applying attack state
        if (theDistance <= _damageStartRadius)
            _weapon.shoot();
    }

    private Vector2 getTargetPosition() {
        return XUtils.isValid(_targetGameObject) ?
            _targetGameObject.transform.position : Vector3.zero;
    }

    private float getCurrentRotation() {
        return transform.rotation.eulerAngles.z;
    }

    //Fields
    [SerializeField] private IWeaponControlInterface _weapon = null;

    [SerializeField] private float _angleForCorrection = 1f;

    [SerializeField] private GameObject _targetGameObject = null;

    private ShipController ship = null;

    [SerializeField] private float _damageStartRadius = 3f;
}
