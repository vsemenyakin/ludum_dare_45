﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Settings/WeaponSettings", order = 1)]
public class WeaponSettings : ScriptableObject
{
    public int maxShellsInClip = 10;
    public float timeSpanToLoadShellFromClipToBarrel = 1.0f;
    public float timeSpanToReloadClip = 2.0f;

    public float damageAmount = 1f;
}
