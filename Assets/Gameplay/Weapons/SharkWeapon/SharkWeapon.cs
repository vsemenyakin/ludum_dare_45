﻿using UnityEngine;

using static DamageSystem;

public class SharkWeapon : IWeaponControlInterface
{   
    public float getDamageRadius() {
        return _damageRadius;
    }

    override public void shoot() {
        FastArray<DamagableZoneQueryResult> theQueryResults;
        using (StackObjects.getTop(out theQueryResults))
        {
            InstigatorComponent theInstigator = _damageSource.instigator;

            queryDamagableZonesInRadius(theQueryResults, transform.position, _damageRadius, theInstigator);

            theQueryResults.filter(new ObjectsWithRelationFilter(theInstigator, Teams.Relations.Enemies));
            foreach (DamagableZoneQueryResult theQueryResult in theQueryResults)
                theQueryResult.damagableZone.durabilityComponent?.changeHitPoints(-_damageAmount);

            if (theQueryResults.getSize() > 0) {
                Vector3 pos = transform.localPosition;
                pos.z = 0;

                GameObject effectPrefab = Instantiate(_effectPrefab);
                effectPrefab.transform.position = transform.position;
                
                //XUtils.destroy(gameObject);

                InstigatorComponent theRootInstigator = theInstigator.getRootInstigator();
                if (XUtils.isValid(theRootInstigator))
                    XUtils.destroy(theRootInstigator);
            }
        }
    }

    private void Awake() {
        XUtils.check(_damageSource);
    }

    override public void setTargetLocation(Vector2 inLocation) { /*No sense for this kind of weapon*/ }

    //Fields
    [SerializeField] private DamageSourceComponent _damageSource = null;

    [SerializeField] private float _damageAmount = 20f;
    [SerializeField] private float _damageRadius = 1f;
    [SerializeField] private GameObject _effectPrefab = null;
}
