﻿using UnityEngine;

using Values;

[ExecuteAlways]
public class LimitedRotatorComponent : MonoBehaviour
{
    public void setTargetLocation(Vector2 inLocation) {
        Vector2 theDirectionLocal = transform.parent.transform.InverseTransformPoint(inLocation);
        _targetRotation = Quaternion.FromToRotation(Vector3.right, theDirectionLocal).eulerAngles.z;
    }

    private void Awake() {
        _rotationValue = new LimitedFloatAngle(_rotationSettings);
    }

    private void FixedUpdate() {
        updateRotationValue();
        applyRotationFromValueToTransform();
    }

    private void updateRotationValue() {
        float theRotationSpeedPerUpdate = _rotationSpeed * Time.fixedDeltaTime;
        _rotationValue.changeAngleToAchieveTargetAngleWithSpeed(_targetRotation, theRotationSpeedPerUpdate);
    }

    private void applyRotationFromValueToTransform() {
        Vector3 theRotationEuler = transform.localRotation.eulerAngles;
        theRotationEuler.z = _rotationValue.getAngle();
        transform.localRotation = Quaternion.Euler(theRotationEuler);
    }

    #region DEBUG_DRAW
#if UNITY_EDITOR
    void Update() {
        if (Application.IsPlaying(this)) return;

        _rotationValue = new LimitedFloatAngle(_rotationSettings);
    }

    private void OnDrawGizmos() {
        Color theColorToSave = Gizmos.color;

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(
            transform.position,
            transform.position + getDebugLimitVectorOffset(_rotationValue.getLimitFrom())
        );

        Gizmos.color = Color.red;
        Gizmos.DrawLine(
            transform.position,
            transform.position + getDebugLimitVectorOffset(_rotationValue.getLimitTo())
        );

        Gizmos.color = theColorToSave;
    }

    private Vector3 getDebugLimitVectorOffset(float inLimitAngle) {
        float theDebugDrawLineLength = 3f;

        Vector3 theDirectionLocal = XMath.getDiractionVectorForAngle(inLimitAngle) * theDebugDrawLineLength;
        return transform.TransformDirection(theDirectionLocal);
    }
#   endif
#   endregion

    //Fields
    [SerializeField] private LimitedFloatAngle.State _rotationSettings = new LimitedFloatAngle.State(45f, true);
    [SerializeField] private float _rotationSpeed = 5f;

    private LimitedFloatAngle _rotationValue;
    private float _targetRotation = 0f;
}
