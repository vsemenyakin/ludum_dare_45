﻿using UnityEngine;

using static XUtils;
using static DamageSystem;

public class GunObject : IWeaponControlInterface
{
    //Methods
    override public void shoot() {
        if (_logic.shoot())
            performShoot();
    }

    override public void setTargetLocation(Vector2 inLocation) {
        _rotator.setTargetLocation(inLocation);
    }

    private void Awake() {
        check(_settings);
        _logic = new WeaponWithClipLogic(_settings);
        
        check(_shootingPoint);

        check(_shootingLine);
        setLineAlpha(0.0f);

        check(_rotator);
    }

    private void setLineAlpha(float inAlpha) {
        Color theColor = _shootingLine.startColor;
        theColor.a = inAlpha;
        _shootingLine.startColor = theColor;

        theColor = _shootingLine.endColor;
        theColor.a = inAlpha;
        _shootingLine.endColor = theColor;
    }

    private void FixedUpdate() {
        _logic.update();
        
        if (_shootingLine.startColor.a > 0) {
            setLineAlpha(_shootingLine.startColor.a - 0.01f);
        } else {
            setLineAlpha(0.0f);
        }
    }
    
    private void performShoot() {
        check(false, "Not implemented!");
        //if(_particleSystem) _particleSystem.Play();
        //
        //DamageInfo theDamageInfo = new DamageInfo();
        //theDamageInfo.damageAmount = _settings.damageAmount;
        //theDamageInfo.instigator = _damageSource.instigator;
        //
        //DamageResult theDamageResult = hitNearestDamagableZone(
        //    _shootingPoint.shootingRay, _settings.maxShootingDistance,
        //   theDamageInfo,
        //    _damageSource.instigator
        //);
        //
        //_shootingLine.SetPosition(0, theDamageResult.damageSourcePoint);
        //_shootingLine.SetPosition(1, theDamageResult.damageFinalPoint);
        //setLineAlpha(1f);
    }

    //Fields
    [SerializeField] private GunSettings _settings = null;
    //[SerializeField, PrefabModeOnly] private DamageSourceComponent _damageSource = null;
    [SerializeField, PrefabModeOnly] private ShootingPointObject _shootingPoint = null;
    [SerializeField, PrefabModeOnly] private LimitedRotatorComponent _rotator = null;
    
    private WeaponWithClipLogic _logic = null;

    [SerializeField] private LineRenderer _shootingLine = null;
    //[SerializeField, PrefabModeOnly] ParticleSystem _particleSystem = null;
}
