﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Settings/GunSettings", order = 1)]
public class GunSettings : WeaponSettings
{
    public float maxShootingDistance = 10.0f;
}
