﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponWithClipLogic
{
    public WeaponWithClipLogic(WeaponSettings inSettings) {
        XUtils.check(inSettings);
        _shellsInClipResource = new ShellsInClipResource(inSettings);
        _shellInBarrelResource = new ShellInBarrelResource(inSettings, _shellsInClipResource);
    }

    public void update() {
        _shellsInClipResource.reset();
        _shellInBarrelResource.reset();

        _shellsInClipResource.update();
        _shellInBarrelResource.update();
    }
    public bool shoot() {
        return _shellInBarrelResource.use();
    }

    public int getShellsInClipNum() {
        return _shellsInClipResource.getShellsInClipNum();
    }

    // - - - - - - - - - - - - - - - - Resources - - - - - - - - - - - - - - - - - - - - - - -

    abstract class Resource
    {
        virtual public void reset() { }
        abstract public void update();
        abstract public bool use();
    }

    class ShellsInClipResource : Resource {
        public ShellsInClipResource(WeaponSettings inSettings) {
            _settings = XUtils.verify(inSettings);
            _shellsInClip = inSettings.maxShellsInClip;
        }
        public int getShellsInClipNum() {
            return _shellsInClip;
        }

        override public void update() {
            if (_reloadTimer.isActive() && _reloadTimer.updateAndCheckIfFired(Time.fixedDeltaTime))
                _shellsInClip = _settings.maxShellsInClip;
        }

        override public bool use() {
            if (_shellsInClip != 0)
            {
                --_shellsInClip;
                return true;
            }

            if (!isReloading())
                _reloadTimer.reset(_settings.timeSpanToReloadClip);

            return false;
        }

        public bool isReloading() {
            return _reloadTimer.isActive();
        }

        //Fields
        private WeaponSettings _settings = null;

        private int _shellsInClip = 0;
        private XTime.Timer _reloadTimer;
    }

    class ShellInBarrelResource : Resource {
        public ShellInBarrelResource(WeaponSettings inSettings, ShellsInClipResource inShellsInClipResource) {
            _settings = XUtils.verify(inSettings);
            _shellsInClipResource = inShellsInClipResource;
        }

        override public void update() {
            if (_loadingShellToBarrelTimer.isActive() && _loadingShellToBarrelTimer.updateAndCheckIfFired(Time.fixedDeltaTime))
                _isShellInBarrel = true;
        }
        
        override public bool use() {
            if (_isShellInBarrel) {
                _isShellInBarrel = false;
                return true;
            }

            if (!isLoadingShellFromClipToBarrel() && _shellsInClipResource.use())
                _loadingShellToBarrelTimer.reset(_settings.timeSpanToLoadShellFromClipToBarrel);

            return false;
        }

        private bool isLoadingShellFromClipToBarrel() {
            return _loadingShellToBarrelTimer.isActive();
        }

        private WeaponSettings _settings = null;

        private ShellsInClipResource _shellsInClipResource = null;

        private bool _isShellInBarrel;
        private XTime.Timer _loadingShellToBarrelTimer;
    }

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    private ShellsInClipResource _shellsInClipResource = null;
    private ShellInBarrelResource _shellInBarrelResource = null;
}
