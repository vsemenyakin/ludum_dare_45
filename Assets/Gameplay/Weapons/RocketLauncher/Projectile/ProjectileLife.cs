﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileLife : MonoBehaviour
{
    [SerializeField] private float _lifeTime = 0f;

    private void Start() {
        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer() {
        yield return new WaitForSeconds(_lifeTime);
        OnTimerEnd();
    }

    private void OnTimerEnd() {
        Destroy(gameObject);
    }

}
