﻿using UnityEngine;

using static DamageSystem;

public class RocketObject : MonoBehaviour
{
    public void init(Quaternion inRotation, InstigatorComponent inInstigator) {
        _instigator.setParentInstigator(inInstigator);
        
        FastArray<Collider2D> theInstigatorDamagableZoneColliders;
        using (StackObjects.getDisposableTop(out theInstigatorDamagableZoneColliders)) {
            _instigator.collectDamagableZoneColliders(theInstigatorDamagableZoneColliders);
            _movement.init(inRotation * Vector2.right, _settings.flyingSpeed,
                DamageSystem.damagableObjectsLayerMask, theInstigatorDamagableZoneColliders
            );
        }
    }
    
    private void Awake() {
        XUtils.verify(_movement).onHitted = onHitted;
        XUtils.check(_instigator);
    }

    private void onHitted(XCollision2D.Hit inHit) {
        FastArray<DamagableZoneQueryResult> theQueryResults;
        using (StackObjects.getDisposableTop(out theQueryResults))
        {
            queryDamagableZonesInRadius(theQueryResults, transform.position, _settings.damageRadius, _instigator);

            theQueryResults.filter(new ObjectsWithRelationFilter(_instigator.getTeam(), Teams.Relations.Enemies));
            foreach (DamagableZoneQueryResult theQueryResult in theQueryResults)
                theQueryResult.damagableZone.durabilityComponent?.changeHitPoints(-_settings.damageAmount);
        }

        XUtils.destroy(gameObject);
    }

    //Fields
    [SerializeField] private RocketSettings _settings = null;
    [SerializeField, PrefabModeOnly] private CircleProjectileMovement _movement = null;
    [SerializeField, PrefabModeOnly] private InstigatorComponent _instigator = null;
}
