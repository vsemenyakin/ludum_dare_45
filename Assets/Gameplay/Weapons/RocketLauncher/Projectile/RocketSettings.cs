﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Settings/RocketSettings", order = 1)]
public class RocketSettings : ScriptableObject
{
    public float flyingSpeed = 10f;
    public float damageRadius = 1f;
    public float damageAmount = 10f;
}
