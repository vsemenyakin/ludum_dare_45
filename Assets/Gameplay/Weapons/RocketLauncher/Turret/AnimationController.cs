﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator _animator = null;
    [SerializeField] private string _animStateName = null;

    private void Start() {
        _animator.enabled = false;
    }

    public void Play() {
        _animator.enabled = true;
        _animator.Play(_animStateName, -1, 0f);
    }

    public void OnAnimationEnd() {
        _animator.enabled = false;
    }
}
