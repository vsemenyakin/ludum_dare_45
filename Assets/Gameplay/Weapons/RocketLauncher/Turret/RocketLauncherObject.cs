﻿using UnityEngine;

using static XUtils;

public class RocketLauncherObject : IWeaponControlInterface
{
    //Methods
    override public void shoot() {
        if (_logic.shoot())
            performShoot();
    }

    override public void setTargetLocation(Vector2 inLocation) {
        _rotator.setTargetLocation(inLocation);
    }

    private void Awake() {
        check(_settings);
        _logic = new WeaponWithClipLogic(_settings);
        
        check(_shootingPoint);
        check(_rotator);
    }

    private void FixedUpdate() {
        _logic.update();
    }
    
    private void performShoot() {
        if (_particleSystem) _particleSystem.Play();
        if (_animator) _animator.Play();
        if (_audio) _audio.PlayOneShot(_audio.clip); 
        RocketObject theRocketObject = createObject(_settings.rocketPrefab);
        theRocketObject.transform.position = _shootingPoint.transform.position;
        theRocketObject.init(_shootingPoint.transform.rotation, _damageSource.instigator);
    }

    //Fields
    [SerializeField] private RocketLauncherSettings _settings = null;
    [SerializeField, PrefabModeOnly] private DamageSourceComponent _damageSource = null;
    [SerializeField, PrefabModeOnly] private ShootingPointObject _shootingPoint = null;
    [SerializeField, PrefabModeOnly] private LimitedRotatorComponent _rotator = null;
    [SerializeField, PrefabModeOnly] private AnimationController _animator = null;
    [SerializeField, PrefabModeOnly] ParticleSystem _particleSystem = null;
    [SerializeField, PrefabModeOnly] AudioSource _audio = null;

    private WeaponWithClipLogic _logic = null;
}
