﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Settings/RocketLauncherSettings", order = 1)]
public class RocketLauncherSettings : WeaponSettings
{
    public RocketObject rocketPrefab = null;
}
