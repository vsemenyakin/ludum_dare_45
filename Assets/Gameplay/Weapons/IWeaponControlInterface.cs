﻿using UnityEngine;

//NB: MonoBehaviour for possibility to serialize objects
public abstract class IWeaponControlInterface : MonoBehaviour
{
    abstract public void shoot();
    abstract public void setTargetLocation(Vector2 inLocation);
}
