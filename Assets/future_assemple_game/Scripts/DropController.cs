﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DropController : MonoBehaviour
{
    private const string DEFAULT_BOX_ICON = "box_default";
    private const int MAX_DROP_COUNT = 10;
    
    private List<DropItemView> _drop = new List<DropItemView>();
    public List<DropItemView> Drop => _drop;

    private Spawner _spawner;

    [SerializeField] private Transform _dropsRect = null;

    private void Awake(){
        _spawner = GetComponentInChildren<Spawner>();
        
        _drop = FindObjectsOfType<DropItemView>().ToList();
        
        var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        gameController.InventoryController.DropPickedUp += HandleDropPickedUp;

        DebugSetWeaponsContent();
    }

    private void Update(){
        if (_drop.Count < MAX_DROP_COUNT){
            SpawnDrop();
        }
    }

    private void SpawnDrop(){
        var dropGO = _spawner.CreateObject(10f);
        if (dropGO != null){
            dropGO.transform.SetParent(_dropsRect);
            var dropView = dropGO.GetComponentInChildren<DropItemView>();
            
            var dropId = _spawner.RandomWeaponId(true);
            dropView.SetContent(dropId);
            _drop.Add(dropView);
        }
    }
    
    public void SpawnDrop(Vector3 position){
        var dropGO = _spawner.CreateObject(10f);
        if (dropGO != null){
            dropGO.transform.position = position;
            dropGO.transform.SetParent(_dropsRect);
            var dropView = dropGO.GetComponentInChildren<DropItemView>();
            
            var dropId = _spawner.RandomWeaponId(true);
            dropView.SetContent(dropId);
            _drop.Add(dropView);
        }
    }
    
    private void RemoveDrop(DropItemView dropItemView){
        _drop.Remove(dropItemView);
        Destroy(dropItemView.gameObject);
    }
    
    private void HandleDropPickedUp(DropItemView dropItemView){
        RemoveDrop(dropItemView);
    }
    
    private void DebugSetWeaponsContent(){
        for (var index = 0; index < _drop.Count; index++) {
            var dropItem = _drop[index];
            /*var random = Random.Range(0, 2);*/
            dropItem.SetContent("cannon");
        }
    }
}
