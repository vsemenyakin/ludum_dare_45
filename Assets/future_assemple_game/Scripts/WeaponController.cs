﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private DataController _dataController;
    private PlayerShipInputController _playerShipInputController;
    
    private void Start() {
        Initialize();

        var backgroundTarget = GameObject.FindWithTag("Background").GetComponent<InventoryItemDropTarget>();
        backgroundTarget.DropReceived += OnBackgroundDropReceived;

    }

    private void Initialize(){
        var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        _dataController = gameController.DataController;
        _playerShipInputController = gameController.PlayerShipInputController;
    }
    
    public bool IsDropWeapon(string _dropId){
        var itemData = _dataController.FindItemData(_dropId);

        WeaponData weaponData = itemData as WeaponData;
        return weaponData != null && weaponData.IsDropWeapon;
    }

    public bool IsWeapon(string id){
        var itemData = _dataController.FindItemData(id);
        return itemData is WeaponData;
    }

    public void AddWeapon(IWeaponControlInterface iWeapon){
        _playerShipInputController.weaponObjects.Add(iWeapon);
        
    }

    public void RemoveWeapon(IWeaponControlInterface iWeapon){
        _playerShipInputController.weaponObjects.Remove(iWeapon);
    }
    
    public static GameObject GetWeaponPrefab(string id){
        var weaponPrefab = Resources.Load<GameObject>($"prefabs/{id}");
        if (weaponPrefab == null){
            Debug.LogError("weapon prefab = " + id + " is not found");
        }
        
        return weaponPrefab;
    }

    private void OnBackgroundDropReceived(string dropWeaponId){
        UseDropWeapon(dropWeaponId);
    }

    private void UseDropWeapon(string dropWeaponId){
        var screenMousePos = Input.mousePosition;
        var worldMousePos = Camera.main.ScreenToWorldPoint(new Vector3(screenMousePos.x, screenMousePos.y, 0f));
        worldMousePos.z = 0;

        var prefab = GetWeaponPrefab(dropWeaponId);
        var weaponGO = Instantiate(prefab);
        weaponGO.transform.position = worldMousePos;

        weaponGO.GetComponent<SharkWeapon>().shoot();
    }
}
