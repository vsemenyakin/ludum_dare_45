﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour {
    public Vector3[] screenBorders = new Vector3[4];
    public GameObject[] spawnPrefabs;
    [SerializeField]
    //private float spawnRange = 10f;
    //[SerializeField]
    //private float spawnDelay = 1f;
    //[SerializeField]
    float freeSpawnRadius = 5f;
    private int screenPart;
    bool isCoroutineExecuting = false;

    private DataController _dataController;

    private void Awake(){
        var gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        _dataController = gameController.DataController;
    }


    void Update() {
        //CreateObject();
    }
    public void CreateObject(float spawnRange = 10f, float spawnDelay = 1f) {
        ScreenBorders();
        screenPart = Random.Range(0, 4);
        if (!isCoroutineExecuting) {
            StartCoroutine(GenerateObject(spawnRange, spawnDelay));
        }
    }

    public GameObject CreateObject(float spawnRange = 10f){
        ScreenBorders();
        screenPart = Random.Range(0, 4);
        return SpawnObject(spawnRange);
    }
    
    void ScreenBorders() {
        screenBorders[0] = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        screenBorders[0].z = 0;
        screenBorders[1] = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
        screenBorders[1].z = 0;
        screenBorders[2] = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        screenBorders[2].z = 0;
        screenBorders[3] = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        screenBorders[3].z = 0;
       // screenPart = Random.Range(0, 4);
    }

    public string RandomWeaponId(bool isIgnoreChance){
        string itemId = null;
        
        int i = 0;
        var items = _dataController.WeaponDatas;
        while (i<3) {
            int randIdItems = Random.Range(0, items.Length);

            if (isIgnoreChance){
                itemId = items[randIdItems].ItemId;
            }
            else{
                throw new NotImplementedException();
//                int randChance = Random.Range(0, 100 + 1);
//                ItemReward reward = items[randIdItems];
//                if (randChance <= reward.rarity) {
//                    itemId = reward;
//                    break;
//                }
            }
            
            
            i++;
            }
        return itemId;
    }

    private GameObject SpawnObject(float spawnRange){
        GameObject spawnObject = null;
        
        float spawnPlaceX = RandomPosX(screenPart, spawnRange);
        float spawnPlaceY = RandomPosY(screenPart, spawnRange);
        if (isFreeSpawnPlace(spawnPlaceX, spawnPlaceY, freeSpawnRadius)) {
            spawnObject = Instantiate(spawnPrefabs[Random.Range(0, spawnPrefabs.Length)], new Vector3(spawnPlaceX, spawnPlaceY, 0), Quaternion.identity);
        }

        return spawnObject;
    }

    IEnumerator GenerateObject (float spawnRange, float spawnDelay) {
        isCoroutineExecuting = true;
        float spawnPlaceX = RandomPosX(screenPart, spawnRange);
        float spawnPlaceY = RandomPosY(screenPart, spawnRange);
        if (isFreeSpawnPlace(spawnPlaceX, spawnPlaceY, freeSpawnRadius)) {
            var spawnObject = Instantiate(spawnPrefabs[Random.Range(0, spawnPrefabs.Length)], new Vector3(spawnPlaceX, spawnPlaceY, 0), Quaternion.identity);
            yield return new WaitForSeconds(spawnDelay);     
        }
        isCoroutineExecuting = false;      
    }

    bool isFreeSpawnPlace (float x, float y, float radius) {
        int a = Physics2D.CircleCastAll(new Vector2(x,y), radius, Vector3.forward).Length;
        if (a >= 1) return false;
        else return true;
    }



    private float RandomPosX(int screenPart, float spawnRange) {
        //int a = Random.Range(0, 4);
        this.screenPart = screenPart;
        float x = 0;
        switch (screenPart) {
            case 0:
                x = Random.Range(screenBorders[0].x, screenBorders[3].x + spawnRange);
                break;
            case 1:
                x = Random.Range(screenBorders[0].x - spawnRange, screenBorders[0].x);
                break;
            case 2:
                x = Random.Range(screenBorders[1].x - spawnRange, screenBorders[2].x);
                break;
            case 3:
                x = Random.Range(screenBorders[3].x, screenBorders[3].x + spawnRange);
                break;
        }
        return x;

    }
    private float RandomPosY(int screenPart, float spawnRange) {
        //int a = Random.Range(0, 4);
        this.screenPart = screenPart;
        float y = 0;
        switch (screenPart) {
            case 0:
                y = Random.Range(screenBorders[3].y - spawnRange, screenBorders[3].y);
                break;
            case 1:
                y = Random.Range(screenBorders[0].y - spawnRange, screenBorders[1].y);
                break;
            case 2:
                y = Random.Range(screenBorders[1].y, screenBorders[1].y + spawnRange);
                break;
            case 3:
                y = Random.Range(screenBorders[3].y, screenBorders[2].y + spawnRange);
                break;
        }
        return y;
    }
}

