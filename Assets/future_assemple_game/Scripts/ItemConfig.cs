﻿using UnityEngine;

[CreateAssetMenu(fileName = "item_config", menuName = "Config/Item", order = 0)]
public class ItemConfig : ScriptableObject
{
    public ItemData ItemData;
}