﻿using UnityEngine;

public class EnemyBeacon : MonoBehaviour
{
    private GameController _gameController;
    
    private void Awake(){
        _gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
        
        var health = GetComponent<DurabilityComponent>();
        health.OnDead += EnemyIsDied;


    }
    
    void EnemyIsDied () {
        _gameController.CountKillEnemy();
    }
}
