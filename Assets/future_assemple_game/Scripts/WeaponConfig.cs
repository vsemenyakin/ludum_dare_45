﻿using UnityEngine;

[CreateAssetMenu(fileName = "weapon_config", menuName = "Config/Weapon", order = 0)]
public class WeaponConfig : ScriptableObject
{
    public WeaponData WeaponData;
}