﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DataType
{
    Item,
    Drop
}

public class DataController : MonoBehaviour
{
    private ItemData[] _itemDatas;

    private WeaponData[] _weaponDatas;
    public WeaponData[] WeaponDatas => _weaponDatas;

    private void Awake(){
        Initialize();
    }

    private void Initialize(){
        var itemConfigs = Resources.LoadAll<ItemConfig>("items/general");
        InitItems(itemConfigs);
        
        var weaponConfigs = Resources.LoadAll<WeaponConfig>("items/weapons");
        InitWeapons(weaponConfigs);
    }

    private void InitItems(ItemConfig[] itemConfigs){
        int itemCount = itemConfigs.Length;
        _itemDatas = new ItemData[itemCount];
        for (int i = 0; i < itemCount; i++){
            _itemDatas[i] = itemConfigs[i].ItemData;
        }
    }

    private void InitWeapons(WeaponConfig[] weaponConfigs){
        int weaponCount = weaponConfigs.Length;
        _weaponDatas = new WeaponData[weaponCount];
        for (int i = 0; i < weaponCount; i++){
            _weaponDatas[i] = weaponConfigs[i].WeaponData;
        }
    }

    private ItemData GetItemData(string itemId){
        foreach (var data in _itemDatas){
            if (data.ItemId == itemId){
                return data;
            }
        }
        return null;
    }

    private WeaponData GetWeaponData(string weaponId){
        foreach (var data in _weaponDatas){
            if (data.ItemId == weaponId){
                return data;
            }
        }
        return null;
    }

    public ItemData FindItemData(string itemId){
        ItemData itemData = GetItemData(itemId);
        
        if(itemData != null)
        {
            return itemData;
        }

        itemData = GetWeaponData(itemId);

        return itemData;
    }
    
    public static Sprite GetItemSprite(DataType type, string id){
        string path = "";
        switch (type){
            case DataType.Item:
                path = $"icons/items/{id}";
                break;
            case DataType.Drop:
                path = $"icons/drops/{id}";
                break;
            default:
                Debug.LogError("DataType is " + type + " is not found");
                break;
        }
        
        return Resources.Load<Sprite>(path);
    }
}
