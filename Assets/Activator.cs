﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator : MonoBehaviour
{
    private void Awake() {
        visualTest.enabled = false;

        if (objectToActivate)
            objectToActivate.SetActive(false);

        if (XUtils.isValid(objectsToActivate))
            foreach (GameObject theObjectToActivate in objectsToActivate)
                theObjectToActivate.SetActive(false);
    }

    void FixedUpdate() {
        timeBeforeActivation -= Time.fixedDeltaTime;
        if (timeBeforeActivation < 0f) {

            if (objectToActivate)
                objectToActivate.SetActive(true);

            if (XUtils.isValid(objectsToActivate))
                foreach (GameObject theObjectToActivate in objectsToActivate)
                    theObjectToActivate.SetActive(true);

            XUtils.destroy(gameObject);
        }
    }

    public SpriteRenderer visualTest = null;
    public GameObject objectToActivate = null;
    public float timeBeforeActivation = 5f;

    public GameObject[] objectsToActivate = null;
}
