﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static partial class ItemsPickingSystem
{
    internal static int pickableObjectsLayer { get { return LayerMask.NameToLayer("Picking"); } }

    internal static int pickableObjectsLayerMask = 1 << pickableObjectsLayer;
}
