﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickerComponent : MonoBehaviour
{
    public void init(float inPickingRadius) {
        _pickingRadius = inPickingRadius;
    }

    public PickableZoneObject getNearestAvailablePickableZone() {
        RaycastHit2D[] theFoundItemHits;
        using (ArraysStack.getTop(out theFoundItemHits, kMaxItemsToFind)) {
            Vector2 theOrigin = transform.position;
            Vector2 theDiraction = Vector2.right;
            float theDistance = 0.01f;

            int theFoundItemsNum = Physics2D.CircleCastNonAlloc(
                theOrigin, _pickingRadius, theDiraction, theFoundItemHits, theDistance,
                ItemsPickingSystem.pickableObjectsLayerMask
            );

            if (0 != theFoundItemsNum) {
                Vector2 thePosition2D = transform.position;

                RaycastHit2D theHearestHit = theFoundItemHits[0];
                float theMinimumDistanceSquare =
                    Vector2.SqrMagnitude(theHearestHit.point - thePosition2D);

                for (int theIndex = 1; theIndex < theFoundItemsNum; ++theIndex) {
                    RaycastHit2D theHit = theFoundItemHits[theIndex];
                    float theDistanceSquare = Vector2.SqrMagnitude(theHit.point - thePosition2D);
                    if (theDistanceSquare < theMinimumDistanceSquare) {
                        theHearestHit = theHit;
                        theMinimumDistanceSquare = theDistanceSquare;
                    }
                }

                return XUtils.getComponent<PickableZoneObject>(
                    theHearestHit.collider, XUtils.AccessPolicy.ShouldExist
                );
            } else {
                return null;
            }
        }
    }

    public PickableZoneObject tryPick() {
        PickableZoneObject theNearestZone = getNearestAvailablePickableZone();
        if (theNearestZone)
            theNearestZone.onPicked(this);

        return theNearestZone;
    }

    //Fields
    [SerializeField] private float _pickingRadius = 0.5f;

    private static int kMaxItemsToFind = 50;
}
