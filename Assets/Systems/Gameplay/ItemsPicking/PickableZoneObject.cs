﻿using UnityEngine;

using static XUtils;

public class PickableZoneObject : MonoBehaviour
{
    private void Awake() {
        check(gameObject.layer == DamageSystem.damagableObjectsLayer);
        check(getComponentInChildren<Collider2D>(this, AccessPolicy.JustFind));
    }

    public System.Action<PickerComponent> onPicked;
}
