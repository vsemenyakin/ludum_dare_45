﻿using UnityEngine;

using static XUtils;

public static class Teams
{
    public enum Relations {
        Neutral,
        Friends,
        Allies,
        Enemies
    }

    public static Optional<Relations> getRelationFor(Object inObjectA, Object inObjectB) {
        return getRelationFor(getTeamFor(inObjectA), getTeamFor(inObjectB));
    }

    public static Optional<Relations> getRelationFor(TeamObject inTeamA, TeamObject inTeamB) {
        if (isValid(inTeamA) && isValid(inTeamB)) {
            return (inTeamA == inTeamB ? Relations.Friends : Relations.Enemies);
        } else {
            return new Optional<Relations>();
        }
    }

    public static TeamObject getTeamFor(Object inObject) {
        if (isValid(inObject)) {
            switch(inObject) {
                case TeamObject theTeam:
                    return theTeam;
                
                default:
                    TeamMemberComponent theTeamMember = getTeamMemberFor(inObject);
                    return isValid(theTeamMember) ? theTeamMember.team : null;
            }   
        } else {
            return null;
        }
    }

    public static TeamMemberComponent getTeamMemberFor(Object inObject) {
        if (isValid(inObject)) {
            switch (inObject) {
                case TeamMemberComponent theTeamMember:  return theTeamMember;
                case Component theComponent:             return getComponent<TeamMemberComponent>(theComponent);
                case GameObject theGameObject:           return getComponent<TeamMemberComponent>(theGameObject);
                default:                                 return null;
            }
        } else {
            return null;
        }
    }
}
