﻿using UnityEngine;

public class TeamMemberComponent : MonoBehaviour
{
    public TeamObject team {
        get { return _team; }
        set { _team = value; }
    }

    [SerializeField] TeamObject _team = null;
}
