﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    private ShipController ship;

    private void Start() {
        ship = gameObject.GetComponent<ShipController>();
        
    }

    private void Update() {
        float move = Input.GetAxis("Vertical");
        float rotate = Input.GetAxis("Horizontal");
        if (move == 0f)
            ship.StopMoving();
        else {
            ship.StartMoving(move);

            float theActualRotate = _isUseInverseOnReverse ?
                ((move > 0f) ? rotate : -rotate) : rotate;

            ship.Rotate(theActualRotate);
        }
    }

    [SerializeField] private bool _isUseInverseOnReverse = true;
}
