﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {
    [SerializeField]
    private float maxSpeed = 1f;
    [SerializeField]
    private float frictionSpeed = 0.5f;
    [SerializeField]
    private float angleRotate = 45f;
    [SerializeField]
    private float speed = 1f;
    private bool isMoving = false;
    //private bool isMovingForward = true;
    private float inputSpeed = 0;

    private new Rigidbody2D rigidbody;

    private Vector2 forwardVector;
    private void Start() 
    {
        UpdateForwardVector();
        //UpdateIsMovingForward();
        rigidbody = gameObject.GetComponent<Rigidbody2D>();
    }
    void Update() {
        //Debug.Log(isMoving);
        //float move = Input.GetAxis("Vertical");
        //float rotate = Input.GetAxis("Horizontal");

        //UpdateIsMovingForward();

        if (isMoving) Move(inputSpeed);

        if(!isMoving) {
            UpdateFriction();
            UpdateStop();
        }

       // Debug.Log(rigidbody.velocity.magnitude);

        UpdateForwardVector();
    }

    Vector2 RotateVector(Vector2 v, float degrees) {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    } 

    void UpdateForwardVector() {
        float x = -Mathf.Sin((transform.eulerAngles.z * Mathf.PI) / 180);
        float y = Mathf.Cos((transform.eulerAngles.z * Mathf.PI) / 180);
        forwardVector = new Vector2(x, y);

    }
    void UpdateFriction() {
        Vector2 friction = (-1) * frictionSpeed * rigidbody.velocity;
        rigidbody.AddForce(friction, ForceMode2D.Impulse);
    }
    void UpdateStop() {
        float minDelta = 0.2f;
        if (rigidbody.velocity.magnitude < minDelta) {
            rigidbody.velocity = Vector2.zero;
            isMoving = false;
        }
    }

   /* private void UpdateIsMovingForward() {
        if (!isMoving) return;
        if (Vector2.Angle(forwardVector, rigidbody.velocity) >= 179 && Vector2.Angle(forwardVector, rigidbody.velocity) <= 181) isMovingForward = false;
        else isMovingForward = true;
    } */

    void Move(float move) {

        //if (move < 0) { StopMoving(); return;}

        
        float curSpeed = rigidbody.velocity.magnitude;
        
        
        if (curSpeed < maxSpeed) {
            rigidbody.AddForce(forwardVector * speed * move, ForceMode2D.Impulse);
        }
        if (rigidbody.velocity.magnitude >= maxSpeed) {
            rigidbody.velocity = rigidbody.velocity * 0.99f;
           // Debug.Log("velos = " + rigidbody.velocity.magnitude);
        }

    }

    public void StartMoving(float inputSpeed) {
        isMoving = true;
        this.inputSpeed = inputSpeed;
    }

    public void StopMoving() {
        isMoving = false;
        inputSpeed = 0f;
    }

    public void Rotate(float b) {
        
        if (rigidbody.velocity.magnitude != 0f) {
            float rotate = b;
            transform.Rotate(0, 0, -rotate * angleRotate * Time.deltaTime * (rigidbody.velocity.magnitude / maxSpeed));
           // Vector2 velocity = rigidbody.velocity;
        }
        //rigidbody.velocity = RotateVector(velocity, -rotate * angleRotate * Time.deltaTime);
    }
}
