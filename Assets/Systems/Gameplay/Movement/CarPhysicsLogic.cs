﻿using UnityEngine;
using System;

using Values;

public class CarPhysicsLogic : MonoBehaviour
{
    //Methods
    public void applyGas() {
        _gasValue.changeValue(GasChangeSpeed);
        _isGasWasChangedOnUpdate = true;
    }

    public void applyRevers() {
        _gasValue.changeValue(-GasChangeSpeed);
        _isGasWasChangedOnUpdate = true;
    }

    public void rotateSteeringWheelClockwise() {
        _steeringWheelValue.changeAngle(-SteeringWheelSpeed);
        _isSteeringWheelWasRotatedOnUpdate = true;
    }

    public void rotateSteeringWheelCounterClockwise() {
        _steeringWheelValue.changeAngle(SteeringWheelSpeed);
        _isSteeringWheelWasRotatedOnUpdate = true;
    }

    public void SetMinimumSpeed(float value) {
        _gasValue.setMinimum(value);
    }
    
    public void SetMaximumSpeed(float value) {
        _gasValue.setMaximum(value);
    }

    //AI helping Accessors

    private void Awake() {
        _gasValue = new LimitedFloat(GasValueSettings);
        _steeringWheelValue = new LimitedFloatAngle(SteeringWheelValueSettings);

        _rigidBody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate() {
        simulate_control();
        simulate_resitances();
    }

    //--Simulation
    //---Control
    private void simulate_control() {
        simulate_control_dissipate();
        simulate_control_apply();
    }

    //----Dissipate
    private void simulate_control_dissipate() {
        simulate_control_dissipate_gas();
        simulate_control_dissipate_steeringWheel();
    }

    private void simulate_control_dissipate_gas() {
        if (!_isGasWasChangedOnUpdate) {
            float theValue = _gasValue.getValue();
            if (Mathf.Abs(theValue) > GasChangeSpeed) {
                _gasValue.changeValue(theValue > 0.0f ? -GasChangeSpeed : GasChangeSpeed);
            } else {
                _gasValue.setValue(0.0f);
            }
            //_gasValue.setValue(0.0f);
        }
        _isGasWasChangedOnUpdate = false;
    }

    private void simulate_control_dissipate_steeringWheel() {
        if (!_isSteeringWheelWasRotatedOnUpdate) {
            _steeringWheelValue.changeAngleToAchieveTargetAngleWithSpeed(0.0f, SteeringWheelReturnSpeed);
        }
        _isSteeringWheelWasRotatedOnUpdate = false;
    }

    //----Apply
    private void simulate_control_apply() {
        simulate_control_apply_gas();
    }

    private void simulate_control_apply_gas() {
        float theGasValue = _gasValue.getValue();
        if (Mathf.Approximately(theGasValue, 0.0f)) return;

        float theSteeringWheelRotation = _steeringWheelValue.getAngle();
        Vector2 theDiraction =  Quaternion.Euler(0f, 0f, theSteeringWheelRotation) * transform.right;

        _rigidBody.AddForceAtPosition(theDiraction * theGasValue,
            transform.TransformPoint(_enginePoint.transform.localPosition)
        );
    }

    //-Resistances
    private void simulate_resitances() {
        Vector2 theVelocity = _rigidBody.velocity;
        float theVelocityMagnitude = theVelocity.magnitude;

        bool theIsMoveForward =
            Quaternion.FromToRotation(transform.right, theVelocity).eulerAngles.z < 90f;

        float theK = theIsMoveForward ?
            _directResistanceCurve.Evaluate(theVelocityMagnitude) :
            _reverseResistanceCurve.Evaluate(theVelocityMagnitude);

        _rigidBody.AddForce(-theVelocity * theK, ForceMode2D.Impulse);

        //Rotation limiting
        //_rotationAngleKPerLinearVelocity
        //SteeringWheelValueSettings

        //NB: We assume that limits are currently same
        if (theIsMoveForward)
        {
            float theMaximumRotationAngle = SteeringWheelValueSettings.LimitTo;
            float theAngleK = _rotationAngleKPerLinearVelocity.Evaluate(
                theVelocityMagnitude / _rotationAngleKPerLinearVelocityMaxValue
            );

            float theActualMaximumRotationAngle = theMaximumRotationAngle * theAngleK;
            _steeringWheelValue.setLimits(-theActualMaximumRotationAngle, theActualMaximumRotationAngle);
        }
    }

    //Fields
    public LimitedFloat.State GasValueSettings = new LimitedFloat.State(-10.0f, 30.0f);
    public float GasChangeSpeed = 0.3f;

    public LimitedFloatAngle.State SteeringWheelValueSettings = new LimitedFloatAngle.State(45.0f, true);
    public float SteeringWheelSpeed = 1.0f;
    public float SteeringWheelReturnSpeed = 2.0f;

    public AnimationCurve _rotationAngleKPerLinearVelocity = null;
    public float _rotationAngleKPerLinearVelocityMaxValue = 10f;

    public AnimationCurve _directResistanceCurve = null;
    public float _directResistanceCurveMaxVelocity = 5f;

    public AnimationCurve _reverseResistanceCurve = null;
    public float _reversResistanceCurveMaxVelocity = 5f;

    public float _maxTorque = 5f;

    //-Runtime
    private LimitedFloat _gasValue;
    private bool _isGasWasChangedOnUpdate = false;

    private LimitedFloatAngle _steeringWheelValue;
    private bool _isSteeringWheelWasRotatedOnUpdate = false;

    public GameObject _enginePoint = null;

    //--Cache
    public Rigidbody2D _rigidBody = null;
}
