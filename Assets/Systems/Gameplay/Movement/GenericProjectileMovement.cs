﻿using UnityEngine;

using static XCollision2D;

public class ProjectileMovement<T_Shape> : MonoBehaviour
    where T_Shape : struct, Shapes.IShape
{
    //Methods
    //-API
    public System.Action<XCollision2D.Hit> onHitted;

    //-Child API
    protected void init(T_Shape inShape, Vector2 inMovementDirection, float inMovementSpeed,
        LayerMask inLayerMaskToTestHit, FastArray<Collider2D> inCollidersToIgnoreOnMove = null)
    {
        _movementShape = inShape;
        _movementDirection = inMovementDirection;
        _movementSpeed = inMovementSpeed;

        _layersToCheckForHit = inLayerMaskToTestHit;
        FastArrayUtils.assignFrom(ref _collidersToIgnoreOnMove, inCollidersToIgnoreOnMove, false);

        if (_rotateUponDirection)
            transform.rotation = Quaternion.FromToRotation(Vector2.right, inMovementDirection);

        gameObject.SetActive(true);

#       if UNITY_EDITOR //TODO: Use here DEBUG_MOVEMENT
        initEditorChecks();
#       endif
    }

    //-Implementation
    private void FixedUpdate() {
#       if UNITY_EDITOR
        updateEditorChecks();
#       endif

        Optional<Hit> theHit = updateHit();
        updateFlying(theHit);
    }

    private Optional<Hit> updateHit() {
        FastArrayUtils.clean(_collidersToIgnoreOnMove);

        _movementShape.center = transform.position;
        Optional<TraceEvent> theHitTraceEvent = trace(_movementShape, velocityPerUpdate, movementTraceSettings);

        return theHitTraceEvent.getField(TraceEvent.hitAccess);
    }

    private void updateFlying(Optional<Hit> inHit)
    {
        if (!inHit.isSet()) {
            Vector3 theVelocityPerUpdate3D = velocityPerUpdate;
            transform.position += theVelocityPerUpdate3D;
        } else {
            transform.position = inHit.value.point;
            onHitted?.Invoke(inHit.value);

            XUtils.destroy(this);
        }
    }

#   if UNITY_EDITOR
    private void initEditorChecks() {
        _isInitialized = true;
    }

    private void updateEditorChecks() {
        XUtils.check(_isInitialized);
    }
#   endif

    private Vector2 velocityPerUpdate {
        get { return _movementDirection * _movementSpeed * Time.fixedDeltaTime; }
    }

    private TraceSettings movementTraceSettings {
        get {
            TraceSettings theSettings = new TraceSettings();

            theSettings.traceEventsToTrackBitMask = new EnumBitMask<TraceEventType>(true);
            theSettings.layerMask = _layersToCheckForHit;
            theSettings.collidersToIgnore = _collidersToIgnoreOnMove;
            theSettings.traceFinishTestDelegate = null;

            return theSettings;
        }
    }

    //Fields
    bool _isInitialized = false;
    private T_Shape _movementShape = new T_Shape();
    private Vector2 _movementDirection = Vector2.zero;
    private float _movementSpeed = 0f;
    private bool _rotateUponDirection = true;

    private LayerMask _layersToCheckForHit = XCollision2D.AllLayers;
    private FastArray<Collider2D> _collidersToIgnoreOnMove = null;
}
