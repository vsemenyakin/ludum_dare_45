﻿using UnityEngine;

using static XUtils;
using static XCollision2D;

public sealed class CircleProjectileMovement : ProjectileMovement<Shapes.Circle>
{
    //Methods
    //-API
    public void init(Vector2 inMovementDirection, float inMovementSpeed,
        LayerMask inLayerMaskToTestHit, FastArray<Collider2D> inCollidersToIgnoreOnMove = null)
    {
        base.init(new Shapes.Circle(Vector2.zero, _radius), inMovementDirection, inMovementSpeed,
            inLayerMaskToTestHit, inCollidersToIgnoreOnMove
        );
    }

    //-Implementation
    private void OnDrawGizmos() {
        gizmosDrawCircle(transform.position, _radius, Color.green);
    }

    private static void gizmosDrawCircle(Vector2 inCenter, float inRadius, Color inColor, int inSegmentsNum = 100) {
        Color theColorToRestore = Gizmos.color;
        Gizmos.color = inColor;

        Quaternion theQuaternion = Quaternion.Euler(0f, 0f, 360f / inSegmentsNum);

        Vector2 theRadiusVector = Vector2.right * inRadius;

        for (int theIndex = 0; theIndex < inSegmentsNum; ++theIndex) {

            Vector2 theLineStartPoint = inCenter + theRadiusVector;
            theRadiusVector = theQuaternion * theRadiusVector;
            Vector2 theLineEndPoint = inCenter + theRadiusVector;

            Gizmos.DrawLine(theLineStartPoint, theLineEndPoint);

            theLineStartPoint = theLineEndPoint;
        }

        Gizmos.color = theColorToRestore;
    }

    //Fields
    [SerializeField] private float _radius = 0.01f;
}
