﻿using UnityEngine;

using static XUtils;

public class DamagableZoneObject : MonoBehaviour
{
    //TODO: Use special "Damage Owner" class instead
    public GameObject owner {
        get { return transform.parent.gameObject; }
    }

    public Collider2D damagableCollider {
        get { return getComponentInChildren<Collider2D>(this, AccessPolicy.ShouldExist); }
    }

    public DurabilityComponent durabilityComponent {
        get { return getComponentInParent<DurabilityComponent>(this); }
    }

    private void Awake() {
        check(gameObject.layer == DamageSystem.damagableObjectsLayer);
        check(damagableCollider);
    }

    public bool _sendDamageToDurability = true;
}
