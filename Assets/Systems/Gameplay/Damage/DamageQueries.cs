﻿using UnityEngine;
using static XUtils;

using static XCollision2D;
using static StackObjectsTypes;

public static partial class DamageSystem
{
    // -------------------------------- Queries ----------------------------------

    public struct DamagableZoneQueryResult {
        public static Optional<DamagableZoneQueryResult> createFor(Optional<TraceEvent> inFinishTraceEvent) {
            if (inFinishTraceEvent.isSet()) {
                DamagableZoneQueryResult theResult;
                theResult.hit = inFinishTraceEvent.value.hit;
                theResult.damagableZone = getComponent<DamagableZoneObject>(theResult.hit.hittedCollider);
                return new Optional<DamagableZoneQueryResult>(theResult);
            } else {
                return new Optional<DamagableZoneQueryResult>();
            }
        }

        public XCollision2D.Hit hit;
        public DamagableZoneObject damagableZone;
    }

    public static Optional<DamagableZoneQueryResult> queryNearestDamagableZoneSweepingShape<T_Type>(
        T_Type inShape, Vector2 inQueryDirection, float inQueryDistance,
        InstigatorComponent inInstigator, FastArray<Collider2D> inAdditionalColliderToIgnore = null)
        where T_Type : struct, Shapes.IShape
    {
        FastArray<Collider2D> TMP_collidersToIgnore;
        using (StackObjects.getDisposableTop(out TMP_collidersToIgnore))
        {
            Vector2 theTraceSweep = inQueryDirection * inQueryDistance;

            TraceSettings theTraceSettings = new TraceSettings();
            theTraceSettings.traceEventsToTrackBitMask = new EnumBitMask<TraceEventType>(true); 
            theTraceSettings.layerMask = damagableReceivingObjectsLayerMask;
            theTraceSettings.collidersToIgnore = TMP_collidersToIgnore;
            collectCollidersToIgnoreFor(theTraceSettings.collidersToIgnore, inInstigator, inAdditionalColliderToIgnore);
            theTraceSettings.traceFinishTestDelegate = null;

            Optional<TraceEvent> theFinishTraceEvent = trace(inShape, theTraceSweep, theTraceSettings);
            return DamagableZoneQueryResult.createFor(theFinishTraceEvent);
        }
    }

    public static void queryDamagableZonesInRadius(FastArray<DamagableZoneQueryResult> outQueryResults,
        Vector2 inCenter, float inRadius,
        InstigatorComponent inInstigator, FastArray<Collider2D> inAdditionalColliderToIgnore = null)
    {
        check(outQueryResults);
        check(lineTracesPerRadialDamage > 0);

        FastArray<Collider2D> TMP_collidersToIgnore;
        FastSet<Collider2D> TMP_currentlyFoundObject;
        using (StackObjects.getDisposableTop(out TMP_collidersToIgnore))
        using (StackObjects.getDisposableTop(out TMP_currentlyFoundObject))
        {
            Shapes.Point theShape = new Shapes.Point(inCenter);
            Vector2 theTraceSweep = Vector2.right * inRadius;

            TraceSettings theTraceSettings = new TraceSettings();
            theTraceSettings.traceEventsToTrackBitMask = new EnumBitMask<TraceEventType>(true);
            theTraceSettings.layerMask = damagableReceivingObjectsLayerMask;
            theTraceSettings.collidersToIgnore = TMP_collidersToIgnore;
            collectCollidersToIgnoreFor(TMP_collidersToIgnore, inInstigator, inAdditionalColliderToIgnore);
            theTraceSettings.traceFinishTestDelegate = null;

            Quaternion theQuaternion = Quaternion.Euler(0f, 0f, 360f / lineTracesPerRadialDamage);

            for (int theLineTraceIndex = 0; theLineTraceIndex < lineTracesPerRadialDamage; ++theLineTraceIndex) {
                Optional<TraceEvent> theFinishTraceEvent = trace(theShape, theTraceSweep, theTraceSettings);
                var theDamagableZoneQueryResult = DamagableZoneQueryResult.createFor(theFinishTraceEvent);

                if (theDamagableZoneQueryResult.isSet()) {
                    Collider2D theHittedCollider = theDamagableZoneQueryResult.value.hit.hittedCollider;
                    if (!TMP_currentlyFoundObject.contains(theHittedCollider)) {
                        outQueryResults.add(theDamagableZoneQueryResult.value);
                        TMP_currentlyFoundObject.add(theDamagableZoneQueryResult.value.hit.hittedCollider);
                    }
                }

                theTraceSweep = theQuaternion * theTraceSweep;
            }
        }
    }

    // -------------------------------- Filters ----------------------------------

    public struct ObjectsWithRelationFilter : FastFunctors.IFilter<DamagableZoneQueryResult>
    {
        public ObjectsWithRelationFilter(Object inObjectToTestRelationFor, Teams.Relations inRequestingRelations) {
            _teamToTestRelationFor = Teams.getTeamFor(inObjectToTestRelationFor);
            _requestingRelations = inRequestingRelations;
        }

        public bool testValue(DamagableZoneQueryResult inValue) {
            TeamObject theValueTeam = Teams.getTeamFor(inValue.damagableZone.owner);
            Optional<Teams.Relations> theRelations = Teams.getRelationFor(_teamToTestRelationFor, theValueTeam);
            return theRelations.isSet() ? (_requestingRelations == theRelations.value) : false;
        }

        private TeamObject _teamToTestRelationFor;
        private Teams.Relations _requestingRelations;
    }

    //Private
    private static void collectCollidersToIgnoreFor(FastArray<Collider2D> outCollidersToIgnore,
        InstigatorComponent inInstigator, FastArray<Collider2D> inAdditionalColliderToIgnore)
    {
        if (isValid(inInstigator))
            inInstigator.collectDamagableZoneColliders(outCollidersToIgnore);

        if (isValid(inAdditionalColliderToIgnore))
            outCollidersToIgnore.addAll(inAdditionalColliderToIgnore);
    }

    internal static int damagableObjectsLayer { get { return LayerMask.NameToLayer("Damage"); } }
    internal static LayerMask damagableObjectsLayerMask = 1 << damagableObjectsLayer;

    internal static int damagableReceivingObjectsLayerMask = damagableObjectsLayerMask;//allLayersMask & ~ignoredObjectsLayerMask;

    private static int lineTracesPerRadialDamage = 50;
}
