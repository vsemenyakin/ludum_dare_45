﻿using UnityEngine;

public static partial class DamageSystem
{
    public struct DamageInfo {
        public float damageAmount;
        public InstigatorComponent instigator;
    }

    public struct DamageResult
    {
        public DamageResult(DamagableZoneObject inDamagedZone,
            Vector2 inDamageSourcePoint, Vector2 inDamageFinalPoint)
        {
            damagedZone = inDamagedZone;
            damageSourcePoint = inDamageSourcePoint;
            damageFinalPoint = inDamageFinalPoint;
        }

        public DamagableZoneObject damagedZone;
        public Vector2 damageSourcePoint;
        public Vector2 damageFinalPoint;
    }
}
