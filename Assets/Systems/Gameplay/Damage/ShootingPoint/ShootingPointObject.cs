﻿using UnityEngine;

public class ShootingPointObject : MonoBehaviour
{
    public Ray2D shootingRay {
        get { return new Ray2D(transform.position, transform.TransformDirection(Vector2.right)); }
    }
}
