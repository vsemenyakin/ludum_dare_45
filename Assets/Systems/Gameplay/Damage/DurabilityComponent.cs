﻿using System;
using UnityEngine;

public class DurabilityComponent : MonoBehaviour
{
    public event Action OnDead;
    public void Awake() {
        _hitPointsValue = new Values.LimitedFloat(
            new Values.LimitedFloat.ZeroBasedState(maxHitPoints, true)
        );
    }

    public float getHitPointsRatio() {
        return _hitPointsValue.getValuePercentFromMinimum();
    }

    public float getHitPoints() {
        return _hitPointsValue.getValue();
    }

    public float setHitPoints(float inHitPoints) {
        float theResult = _hitPointsValue.setValue(inHitPoints);
        processAutodestroy();
        return theResult;
    }
    
    public float changeHitPoints(float inHitPointsDelta) {
        float theResult = _hitPointsValue.changeValue(inHitPointsDelta);
        processAutodestroy();
        return theResult;
    }

    private void processAutodestroy() {
        if (_hitPointsValue.isValueMinimum()) {
            if (_autodestroyOnNoHitPoints) {
                if (_destroyEffectPrefab) {
                    GameObject theObject = Instantiate(_destroyEffectPrefab);

                    theObject.transform.position = transform.position;
                    theObject.transform.rotation = transform.rotation;
                   // theObject.transform.localScale = transform.localScale;
                }

                OnDead?.Invoke();
                
                XUtils.destroy(gameObject);
            }
        }
    }

    [SerializeField] private float maxHitPoints = 100f;
    public float MaxHitPoints => maxHitPoints;

    private Values.LimitedFloat _hitPointsValue;

    public bool _autodestroyOnNoHitPoints = true;
    [SerializeField] GameObject _destroyEffectPrefab = null;

    public bool showLifebarForUI = true;
}
