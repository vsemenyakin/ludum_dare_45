﻿using UnityEngine;
using static XUtils;

public class InstigatorComponent : MonoBehaviour
{
    public void setParentInstigator(InstigatorComponent inParentInstigator) {
        _parentInstigator = inParentInstigator;
    }

    public InstigatorComponent parentInstigator {
        get { return _parentInstigator; }
    }

    public TeamObject getTeam() {
        TeamObject theTeam = Teams.getTeamFor(this);
        return theTeam ? theTeam : (parentInstigator ? parentInstigator.getTeam() : null);
    }

    public InstigatorComponent getRootInstigator() {
        InstigatorComponent theInstigator = this;
        for (; isValid(theInstigator.parentInstigator); theInstigator = theInstigator.parentInstigator);
        return theInstigator;
    }

    public void collectDamagableZones(FastArray<DamagableZoneObject> outDamagableZones) {
        if (isValid(_parentInstigator))
            _parentInstigator.collectDamagableZones(outDamagableZones);

        collectComponentsInChildren(outDamagableZones, gameObject);
    }

    public void collectDamagableZoneColliders(FastArray<Collider2D> outDamagableZoneColliders) {
        FastArray<DamagableZoneObject> theCollectedDamagableZones;
        using (StackObjects.getDisposableTop(out theCollectedDamagableZones))
        {
            collectDamagableZones(theCollectedDamagableZones);

            foreach (DamagableZoneObject theCollectedDamagableZone in theCollectedDamagableZones)
                outDamagableZoneColliders.add(theCollectedDamagableZone.damagableCollider);
        }
    }

#   if UNITY_EDITOR
    private void Awake() {
        //TODO: Check just in parent, not include This
        //check(!getComponentInParent<InstigatorComponent>(this, AccessPolicy.JustFind));
    }
#   endif

    private InstigatorComponent _parentInstigator = null;
}
