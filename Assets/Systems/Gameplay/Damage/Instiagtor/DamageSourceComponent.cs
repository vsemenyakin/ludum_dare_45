﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSourceComponent : MonoBehaviour
{
    //TODO: May be cached
    public InstigatorComponent instigator {
        get { return XUtils.getComponentInParent<InstigatorComponent>(this, XUtils.AccessPolicy.ShouldExist); }
    }
}
