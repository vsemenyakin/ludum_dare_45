﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAttach : MonoBehaviour
{
    private void Start() {
        if (!_camera) _camera = Camera.main;
    }
    void FixedUpdate() {
        Vector3 thePosition = transform.position;
        thePosition.z = _camera.transform.position.z;
        _camera.transform.position = thePosition;
    }

    public Camera _camera = null;
}
