﻿using UnityEngine.EventSystems;

public interface IDropTarget {
	/// <summary>
	/// Is a dropable suitable to be dropped here?
	/// </summary>
	/// <param name="draggable"></param>
	/// <returns></returns>
	bool CanBeDropped(IDroppable droppable, PointerEventData eventData);

	/// <summary>
	/// React to a drop being above a target.
	/// </summary>
	void GiveDropFeedback(IDroppable droppable, PointerEventData eventData);

	/// <summary>
	/// Receive a dropable object.
	/// </summary>
	void ReceiveDrop(IDroppable droppable, PointerEventData eventData);
}