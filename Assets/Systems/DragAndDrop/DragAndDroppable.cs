﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDroppable : MonoBehaviour,
        IPointerDownHandler,
        IBeginDragHandler,
        IPointerExitHandler,
        IDragHandler,
        IEndDragHandler {
        #region Parameters

        [SerializeField]
        public Vector3 offsetPosition;

        #endregion Parameters

        #region State

        /// <summary>
        /// Only a single item can be dragged at a time.
        /// </summary>
        [NonSerialized]
        static DragAndDroppable _active;

        [NonSerialized]
        IDroppable _iDroppable;

        IDroppable Droppable {
            get {
                if (_iDroppable == null)
                    _iDroppable = this.GetComponent<IDroppable>();
                return _iDroppable;
            }
        }

        /// <summary>
        /// The starting position.
        /// </summary>
        [NonSerialized]
        Vector3 startPosition;

        [NonSerialized]
        Vector3 startLocalPosition;

        /// <summary>
        /// The starting parent.
        /// </summary>
        [NonSerialized]
        Transform startParent;

        [NonSerialized]
        Canvas parentCanvas;

        /// <summary>
        /// The original drag camera to use.
        /// </summary>
        [NonSerialized]
        new Camera camera;

        /// <summary>
        /// The initial drag offset.
        /// </summary>
        [NonSerialized]
        Vector3 dragOffset;

        [NonSerialized]
        bool blocksRaycastsSave;

        [NonSerialized]
        bool raycastTargetSave;

        [NonSerialized]
        RectTransform rectTransform;

        // A 3D world plane of dragging.
        [NonSerialized]
        Plane plane;

        [NonSerialized]
        public bool IsAligned = true;

        #endregion State

        #region State

        /// <summary>
        /// Get the global pointer position.
        /// </summary>
        /// <param name="screenPos">
        /// A screen position of the pointer.
        /// </param>
        /// <returns>
        /// A position in the world within a dragging plane.
        /// </returns>
        Vector3 GetPointerWorldPosition(Vector2 screenPos) {
        
            camera = Camera.main;
                return (Vector3)screenPos;
        /*if (this.camera == null) {
                throw new InvalidOperationException("No dragging camera was set or found for: " + this);
            }

            var screenPos3D = (Vector3) screenPos;
            
            screenPos3D.z = this.plane.GetDistanceToPoint(this.camera.transform.position);
        
        var worldPos = this.camera.ScreenToWorldPoint(screenPos3D);
        worldPos.z = 0;
        Debug.Log(gameObject.name);
        return (Vector3)screenPos;*/
        }

        #endregion State

        #region Position

        /// <summary>
        /// Revert to the drag starting position.
        /// </summary>
        public virtual void Revert() {
            transform.position = startPosition;

            if (startParent == null)
                this.transform.parent = null;
            else {
                this.transform.SetParent(startParent, worldPositionStays: true);
                transform.localPosition = startLocalPosition;
            }

            Destroy(emptyObject);
            transform.SetSiblingIndex(startSibling);
        }

        #endregion Position

        #region IBeginDragHandler

        private int startSibling;
        private GameObject emptyObject;

        public virtual void OnPointerDown(PointerEventData eventData) {
            StartDetectLongTap();
        }

        public virtual void OnPointerExit(PointerEventData eventData) { }


        public virtual void OnBeginDrag(PointerEventData eventData) {
            if (_active != null) {
                return;
            }

            _active = this;

            startSibling = transform.GetSiblingIndex();
            startParent = transform.parent;
            startPosition = transform.position;
            startLocalPosition = transform.localPosition;

            camera = null;

            parentCanvas = GetComponentInParent<Canvas>();

            if (parentCanvas) {
                emptyObject = new GameObject("empty_space_crunch");
                if (rectTransform == null) {
                    rectTransform = GetComponent<RectTransform>();
                }

                if (rectTransform != null) {
                    emptyObject.AddComponent<RectTransform>().sizeDelta = rectTransform.sizeDelta;
                }

                emptyObject.transform.SetParent(startParent);
                emptyObject.transform.SetSiblingIndex(startSibling);

                transform.SetParent(parentCanvas.transform);
                transform.SetAsLastSibling();

                //end crunch

                if (parentCanvas.renderMode != RenderMode.ScreenSpaceCamera) {
                    return;
                }

                camera = parentCanvas.worldCamera;
                if (!camera) {
                    return;
                }
            } else {
                // A draggable is in a 3D world, not canvas.
                camera = Camera.main;
            }


            if (this.camera == null) {
                throw new Exception("No camera was found during the start of a drag: " + this);
            }

            // Create a world plane for dragging.
            plane = new Plane(-camera.transform.forward,
                startPosition);

            var startPointerPos = GetPointerWorldPosition(eventData.position);

            dragOffset = startPosition - startPointerPos + offsetPosition;
        }

        #endregion IBeginDragHandler

        #region IDragHandler

        private Vector3 _targtetPointerPos;
        public virtual void OnDrag(PointerEventData eventData) {
            //need because some times OnDrag can call before OnBeginDrag
            if (_active != this)
                return;

            var pickedGO = eventData.pointerCurrentRaycast.gameObject;
            if (pickedGO != null) {
                var iDropTarget = pickedGO.GetComponent<IDropTarget>();
                if (iDropTarget != null
                    && Droppable != null)
                    iDropTarget.GiveDropFeedback(Droppable, eventData);
            }

            _targtetPointerPos = GetPointerWorldPosition(eventData.position);

            this.transform.position = _targtetPointerPos + this.dragOffset;
        }

        #endregion IDragHandler

        protected virtual void Update() {
            if (_isLongTapActive) {
                LongTap();
            }
            
            if (_active != this) {
                return;
            }

            if (Input.touches.Length > 1) {
                CancelDrag();
                return;
            }

            if (IsAligned) {
                this.dragOffset = Vector3.Lerp(dragOffset, offsetPosition, Time.deltaTime * 2f);
            }
            this.transform.position = _targtetPointerPos + this.dragOffset;
        }

        protected void OnApplicationFocus(bool hasFocus) {
            if (_active != this) {
                return;
            }
            
            CancelDrag();
        }

        public void CancelDrag() {
            StopDrag();
            OnEndDrag(new PointerEventData(EventSystem.current));
            Revert();
        }

        #region LongTap
        [Header("Long Tap")]
        public float LongTapTargetTime;
        private float _longTapElapsedTime;
        private bool _isLongTapActive;
        protected bool _isLongTapPerformed;
        
        private void LongTap() {
            // Some longTap stuff code here
            if (!_isLongTapActive) {
                return;
            }

            if (Input.GetMouseButtonUp(0)) {
                StopLongTap();
                return;
            }

            _longTapElapsedTime += Time.deltaTime;
            if (_longTapElapsedTime >= LongTapTargetTime) {
                CompleteLongTap();
            }
        }
        
        private void StartDetectLongTap() {
            _isLongTapActive = true;
            _isLongTapPerformed = false;

            _longTapElapsedTime = 0.0f;
        }

        private void StopLongTap() {
            _isLongTapActive = false;
        }

        protected virtual void CompleteLongTap() {
            _isLongTapActive = false;
            _isLongTapPerformed = true;
        }
        #endregion

        #region IEndDragHandler

        public virtual void OnEndDrag(PointerEventData eventData) {
            if (_active != this) {
                return;
            }
            _active = null;
            var pickedGO = eventData.pointerCurrentRaycast.gameObject;

            var raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raycastResults);


            foreach (var result in raycastResults) {
                if (result.gameObject.GetComponent<IDropTarget>() != null) {
                    pickedGO = result.gameObject;
                    break;
                }
            }

            if (pickedGO != null) {
                var iDropTarget = pickedGO.GetComponent<IDropTarget>();

                if (iDropTarget != null
                    && Droppable != null
                    && iDropTarget.CanBeDropped(Droppable, eventData)
                    && Droppable.CanBeDroppedOn(iDropTarget, eventData)) {

                    Droppable.DropOn(iDropTarget, eventData);
                    iDropTarget.ReceiveDrop(Droppable, eventData);
                    Revert();
                } else {
                    Revert();
                }
            } else {
                Revert();
            }
        }

        public void StopDrag() {
            _active = null;
        }

        #endregion IEndDragHandler
    }

