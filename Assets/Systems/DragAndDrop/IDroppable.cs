﻿using UnityEngine.EventSystems;

/// <summary>
/// An interface for something,
/// that can be dropped.
/// </summary>
public interface IDroppable {
	/// <summary>
	/// Can droppable be dropped on a target?
	/// </summary>
	/// <param name="target"></param>
	/// <returns></returns>
	bool CanBeDroppedOn(IDropTarget target, PointerEventData eventData);

	/// <summary>
	/// Be dropped on a target.
	/// </summary>
	/// <param name="target"></param>
	void DropOn(IDropTarget target, PointerEventData eventData);
}