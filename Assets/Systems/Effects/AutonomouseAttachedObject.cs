﻿using UnityEngine;

public class AutonomouseAttachedObject : MonoBehaviour {
    private void Start() {
        _transformToAttach = transform.parent;
        transform.parent = null;
    }

    private void FixedUpdate() {
        if (XUtils.isValid(_transformToAttach)) {
            Vector3 thePosition = _transformToAttach.position;
            if (_saveOriginalZPosition)
                thePosition.z = transform.position.z;

            transform.position = thePosition;
            transform.rotation = _transformToAttach.rotation;
        }

        var theParticleSystem = XUtils.getComponent<ParticleSystem>(this);
        if (theParticleSystem && !theParticleSystem.IsAlive())
        {
            Destroy(gameObject);
        }
    }

    private Transform _transformToAttach = null;
    private bool _saveOriginalZPosition = true;
}
