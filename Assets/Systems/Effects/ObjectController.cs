﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    [SerializeField] private GameObject _gameObject = null;
    [SerializeField] private GameObjectBuffer _buffer = null;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z)) {
            _gameObject.SetActive(true);
        }
        if (Input.GetKey(KeyCode.X))
        {
            _gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.Space)) {
            _gameObject.GetComponent<ExplosionAnimatorController>().BlowUp(_buffer);
        }

        if (Input.GetKey(KeyCode.Q)) {
            GameObject obj = _buffer.Pop();
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            obj.transform.position = pos;
            obj.GetComponent<ExplosionAnimatorController>().BlowUp(_buffer);
        }
    }
}
