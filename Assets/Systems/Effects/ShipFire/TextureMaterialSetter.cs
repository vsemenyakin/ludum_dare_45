﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMaterialSetter : MonoBehaviour {

    private void Start() {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.material.mainTexture = spriteRenderer.sprite.texture;
    }
}
