﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiffuseTransitionUpdater : MonoBehaviour {
    private DurabilityComponent _durability;
    private SpriteRenderer _sprite;
    private void Start() {
        _durability = GetComponentInParent<DurabilityComponent>();
        _sprite = GetComponent<SpriteRenderer>();
        //_sprite.material.SetFloat("DiffuseTransition", 0);
    }

    private void Update() {
        float f = 1 - _durability.getHitPoints() / _durability.MaxHitPoints;
        _sprite.material.SetFloat("Vector1_55CEB72", 1 - _durability.getHitPoints() / _durability.MaxHitPoints);
    }
}
