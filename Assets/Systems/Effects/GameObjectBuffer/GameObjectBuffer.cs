﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectBuffer : MonoBehaviour{

    [SerializeField] private int _startBufSize = 20;
    [SerializeField] private GameObject _prefabTemplate = null;

    private Stack<GameObject> _buffer = new Stack<GameObject>();

    private void Start() {
        for(int cnt = 0; cnt < _startBufSize; cnt++) {
            _buffer.Push(InstantiatePrefab());
        }
    }

    public GameObject Pop() {
        if (_buffer.Count == 0) return InstantiatePrefab();
        return _buffer.Pop();
    }

    public void Push(GameObject obj) {
        _buffer.Push(obj);
    }

    private GameObject InstantiatePrefab() {
        GameObject obj = Instantiate(_prefabTemplate);
        obj.GetComponent<BuferSetter>().SetBuffer(this);
        return obj;
    }
}
