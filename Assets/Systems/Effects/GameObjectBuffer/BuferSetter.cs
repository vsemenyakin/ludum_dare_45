﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuferSetter : MonoBehaviour
{
    public GameObjectBuffer _buffer;
    

    public GameObjectBuffer GetBuffer() {
        return _buffer;
    }

    public void SetBuffer(GameObjectBuffer obj) {
        _buffer = obj;
    }
}
