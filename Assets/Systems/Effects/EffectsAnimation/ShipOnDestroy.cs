﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipOnDestroy : MonoBehaviour
{
    [SerializeField] private GameObject _audio = null;
    private void Start() {
        GameObject obj = Instantiate(_audio, transform, true);
        obj.GetComponent<ExplosuonAudioController>().SetParent(gameObject);
    }
    public void OnAnimationEnd() {
        //Debug.Log(transform.position);
        GameObject.FindGameObjectWithTag("DropController").GetComponent<DropController>().SpawnDrop(transform.localPosition);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Animator>().enabled = false;
        //Destroy(gameObject);
    }
}
