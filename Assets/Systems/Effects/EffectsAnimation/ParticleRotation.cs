﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRotation : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        var theParticleSystem = GetComponent<ParticleSystem>();
        ParticleSystem.MainModule theMainModule = theParticleSystem.main;
        theMainModule.startRotation = 90 - transform.rotation.eulerAngles.z / (180.0f / Mathf.PI);
    }
}
