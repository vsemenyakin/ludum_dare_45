﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosuonAudioController : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;
    [SerializeField] private GameObject _parent;

    private void Start() {
        _audio = GetComponent<AudioSource>();
    }

    void Update() {
        if (!_audio.isPlaying) {
            Destroy(_parent);
            Destroy(gameObject);
        }
    }

    public void SetParent(GameObject obj) {
        _parent = obj;
    }
}
