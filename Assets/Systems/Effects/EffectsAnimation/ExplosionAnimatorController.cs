﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAnimatorController : MonoBehaviour {

    [SerializeField]//Added in Editor
    private Animator _animator = null;
    [SerializeField]//Added in Editor
    private SpriteRenderer _spriteRenderer = null;
    public GameObjectBuffer _buffer = null;

    private void Start() {
        _animator.enabled = false;
        _spriteRenderer.enabled = false;
    }

    public void BlowUp(GameObjectBuffer buffer) {
        _buffer = buffer;
        _spriteRenderer.enabled = true;
        _animator.enabled = true;
        _animator.Play("Explosion", -1, 0f);
    }

    public void OnAnimationEnd() {
        _buffer.Push(gameObject);
        _animator.enabled = false;
        _spriteRenderer.enabled = false;
        
    }
}
